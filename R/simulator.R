require(animation)
require(plotrix)
require(plyr)

data(lexicon)

.nWords <- 500 # hard coded into constants.h at the moment
.nNonwords <- 500 # hard coded into constants.h at the moment
.stringsPerTrial <- 6
.probWordTrial <- 0.5

.archParams <- list(eyeBrainLagMean=50, saccadeExecTimeMean = 40, saccadeProgTimeMean = 100, motorTimeMean= 250, coefVariation=0.3, perceptualPrimacy=TRUE, timePerStep=10)
.polParams <- list(saccadeA=0.99, saccadeB=0, memoryA=0, memoryB=0, taskYes=0.99, taskNo=0.99, conditioningPost = 1, maxThresh=0.9999999999, sampDeadline=100)
.samplerParams <- list(perceptNoise = 1.2, memNoise = 1.3)
.beliefParams <- list(trialWordPrior = 0.5, conditioningPost = 1, updateNoise=0.5)
.trialParams <- list(maxTrialDuration = 20000)

.traceRequirements <- list(function(stateTrace, actionTrace) return(TRUE))
.maxTime = 3000

loadWords <- function(wordsAndFreqs=.wordsAndFreqs, nWords){

  pickedWords <- sample.int(nrow(wordsAndFreqs), size=nWords)
  words <- wordsAndFreqs[pickedWords,1]
  freqs <- wordsAndFreqs[pickedWords,2]
  return(list(words=words, freqs=freqs))
}

loadNonwords <- function(nonwordSet=.nonwordSet, nNonwords){
  pickedNonwords <- sample.int(length(nonwordSet), size=nNonwords)
  nonwords <- nonwordSet[pickedNonwords]
  return(nonwords)
}

setupSim <- function(.archParams, .polParams, .samplerParams, .beliefParams, .trialParams, .words, .freqs, .nonwords ){
  allParams <- list(archParams = .archParams, polParams = .polParams, samplerParams = .samplerParams, beliefParams = .beliefParams, words = .words, nonwords = .nonwords, freqs = .freqs, trialParams = .trialParams)
  trial <- new(Trial, allParams)

  return(trial)
}

generateTrialInfo <- function(words, nonwords, nWords, nNonwords){
  trialType <- rbinom(1,1,.probWordTrial)
  if(trialType == 1) { # word
    pickedStrings <- sample(sample.int(nWords, .stringsPerTrial)) # shuffle
    trialStrings <- words[pickedStrings]
    # recorder$setTrialInfo(pickedStrings, 1)
  } else{
    pickedWords <- sample.int(nWords, .stringsPerTrial-1)
    nonword <- sample.int(nNonwords,1)
    nonword.pos <- sample.int(.stringsPerTrial, 1)
    pickedStrings <- sample(c(pickedWords, -1))
    # recorder$setTrialInfo(pickedStrings, 0)
    trialStrings <- sapply(pickedStrings, function(x) if(x==-1) nonwords[nonword] else words[x])
  }
  return(list(ts=trialStrings, tt=trialType))
}

simulateTrial <- function(traceRequirements=.traceRequirements, wordsAndFreqs=.wordsAndFreqs, nonwordSet=.nonwordSet, nWords=.nWords, nNonwords=.nNonwords, archParams=.archParams, polParams=.polParams, samplerParams=.samplerParams, beliefParams=.beliefParams, trialParams=.trialParams, maxAttempts = 100){
  wordAndFreqSet <- loadWords(wordsAndFreqs, nWords)
  words <- wordAndFreqSet$words
  freqs <- wordAndFreqSet$freqs
  nonwords <- loadNonwords(nonwordSet, nNonwords)
  trial <- setupSim(archParams, polParams, samplerParams, beliefParams, trialParams, words, freqs, nonwords)
  for (i in 1:maxAttempts){
    trialInfo <- generateTrialInfo(words, nonwords, nWords, nNonwords)
    trial$run(trialInfo$ts, trialInfo$tt)
    passesRequirements <- sapply(traceRequirements, function(x) x(trial$stateTrace, trial$actionTrace))
    if(all(passesRequirements)) break
  }
  if(all(passesRequirements)){
    return(list(trialType=trialInfo$tt, trialStrings = trialInfo$ts, trace = trial$getTrace()))
  } else{
    stop("No trial passes requirements. Check your requirements functions or run more trials!")
  }
}

staticPlot <- function(traces, maxTime=.maxTime){
  beliefTrace <- traces$trace$beliefTrace
  stageTrace <- traces$trace$wordLevelTrace[,1:16] # don't need samps to threshold here
  response <- traces$trace$response
  if(response == -10000){
    responseLabel <- "No response"
    motorStart <- maxTime-200
  } else {
    motorStart <- traces$trace$motorStart
    motorEnd <- traces$trace$motorEnd
    if(response==0){
      responseLabel <- "Not all words"
    } else if (response==1){
      responseLabel <- "All words"
    }
  }
  # -10000 is our dummy initializer. if eblIn is -10000, we haven't read that string
  stringsRead <- sum(stageTrace$volitionalPerSampStart!=-10000)
  # toss out words we never got to
  stageTrace <- stageTrace[1:stringsRead,]
  stageTrace[stageTrace==-10000] <- motorEnd
  trialLabel <- ifelse(traces$trialType==1, "WORD", "NONWORD")
  accLabel <- ifelse(response==traces$trialType, "CORRECT", "INCORRECT")
  eblInBounds <- cbind(stageTrace$eblInStart, stageTrace$eblInEnd)
  eblOutBounds <- cbind(stageTrace$eblOutStart, stageTrace$eblOutEnd)
  setBounds <-  cbind(stageTrace$setStart, stageTrace$setEnd)
  sptBounds <-  cbind(stageTrace$sptStart, stageTrace$sptEnd)
  volitionalPerSampBounds <- cbind(stageTrace$volitionalPerSampStart, stageTrace$volitionalPerSampEnd)
  freePerSampBounds <- cbind(stageTrace$freePerSampStart, stageTrace$freePerSampEnd)
  memSampBounds <- cbind(stageTrace$memSampStart, stageTrace$memSampEnd)
  droppedSampBounds <- cbind(stageTrace$sampDropStart, stageTrace$sampDropEnd)
  fixBounds <- cbind(stageTrace$eblInStart, stageTrace$setStart)
  
  setupPlot(accLabel, trialLabel, .polParams, .samplerParams, .maxTime, traces$trialStrings[1:stringsRead], stageTrace$eblInStart, stageTrace$volitionalPerSampStart, stageTrace$freePerSampEnd)
  plotEBL(eblInBounds)
  plotEBL(eblOutBounds)
  plotPerSampling(volitionalPerSampBounds)
  plotPerSampling(freePerSampBounds)
  plotSacProg(sptBounds)
  plotSacExec(setBounds)
  if(any(memSampBounds[,1]-memSampBounds[,2]!=0)){
    plotMemSampling(memSampBounds)
  }
  if(any(droppedSampBounds[,1]-droppedSampBounds[,2]!=0)){
    plotDroppedSamples(droppedSampBounds)
  }
  plotFixations(fixBounds)
  plotMotor(c(motorStart,motorEnd))
  # get all bounds containing sampling
  perSampBounds <- cbind(volitionalPerSampBounds[,1], freePerSampBounds[,2])
  # get them in order
  perSampBounds <- perSampBounds[order(perSampBounds[,1]),,drop=FALSE]
  memSampBounds <- memSampBounds[order(memSampBounds[,1]),,drop=FALSE]
  # subtract a bit because seq() is inclusive and we want exclusive of end
  perSampBounds[,2] <- perSampBounds[,2] - 0.0001
  memSampBounds[,2] <- memSampBounds[,2] - 0.0001
  # retain only ones with positive sampling remaining: 
  perSampBounds <- perSampBounds[perSampBounds[,2]>perSampBounds[,1],,drop=FALSE]
  memSampBounds <- memSampBounds[memSampBounds[,2]>memSampBounds[,1],,drop=FALSE]
  # run seq to obtain sample times. perSamp will always have at least one sample...
  perSampTimes <- unlist(alply(perSampBounds, 1, function(x) seq(x[1], x[2], by=10)))
  # ...but memSamp might not, so check here: 
  if (length(memSampBounds)>0){
    memSampTimes <- unlist(alply(memSampBounds, 1, function(x) seq(x[1], x[2], by=10)))
  } else{
    memSampTimes <- c()
  }
  # just make sure our length is ok
  stopifnot((length(perSampTimes)+ length(memSampTimes)) == ncol(beliefTrace))
  # now tag each sample with whether it's perception or memory
  allSampTimes <- data.frame(times=perSampTimes, type='Perception')
  if (length(memSampTimes)>0){
    allSampTimes <- rbind(allSampTimes, data.frame(times=memSampTimes, type='Memory'))
  }
  # and sort chronologically
  allSampTimes <- allSampTimes[order(allSampTimes$times),]

  plotTrialBeliefs(beliefTrace[1,], allSampTimes)
  plotPositionBeliefs(beliefTrace[-1,], allSampTimes)
  text(label=responseLabel,cex=1.8,x=maxTime-500,y = 1.2, pos=4,col="red")
  
}


animPlot <- function(traces, stringsPerTrial=.stringsPerTrial, maxTime=.maxTime, polParams=.polParams, samplerParams=.samplerParams, outputDir=getwd(), name="LLDT_animation", type="HTML"){
  positionBeliefs <- traces$stateTrace[,1:stringsPerTrial]
  trialBeliefs <- traces$stateTrace[,1+stringsPerTrial]
  eblInF <- as.logical(traces$stateTrace[,2+stringsPerTrial])
  eblOutF <- as.logical(traces$stateTrace[,3+stringsPerTrial])
  setF <- as.logical(traces$stateTrace[,4+stringsPerTrial])
  sptF <- as.logical(traces$stateTrace[,5+stringsPerTrial])
  perSampF <- as.logical(traces$stateTrace[,6+stringsPerTrial])
  memSampF <- as.logical(traces$stateTrace[,7+stringsPerTrial])
  eblInTimer <- traces$stateTrace[,8+stringsPerTrial]
  eblOutTimer <- traces$stateTrace[,9+stringsPerTrial]
  sptTimer <- traces$stateTrace[,10+stringsPerTrial]
  setTimer <- traces$stateTrace[,11+stringsPerTrial]
  eblOutNext <- traces$stateTrace[,12+stringsPerTrial]
  sfd <- traces$stateTrace[,13+stringsPerTrial]
  trialTimes <- traces$stateTrace[,14+stringsPerTrial]
  focus <- traces$stateTrace[,15+stringsPerTrial]
  motorTime <- traces$stateTrace[,16+stringsPerTrial]
  sampType <- traces$stateTrace[,17+stringsPerTrial]

  par(lend=1) ## butt line caps
if(is.na(which(traces$actionTrace[,2]!=0)[1])){ # no motor action
    responseLabel <- "No response"
    response <- 2
    motorStart <- maxTime-200
  } else {
    motorStart <- which(traces$actionTrace[,2]!=0)[1]
    motorAction <- traces$actionTrace[motorStart,2]
    if(motorAction==2){
      responseLabel <- "Not all words"
      response <- 0
    } else if (motorAction==1){
      responseLabel <- "All words"
      response <- 1
    }
  }
  trialLabel <- ifelse(traces$trialType==1, "WORD", "NONWORD")
  accLabel <- ifelse(response==traces$trialType, "CORRECT", "INCORRECT")
  eblInBounds <- boundaries(eblInF)
  eblOutBounds <- boundaries(eblOutF)
  setBounds <-  boundaries(setF)
  sptBounds <-  boundaries(sptF)
  perSamplingBounds <- boundaries(perSampF)
  memSamplingBounds <- boundaries(memSampF)
  droppedSampBounds <- boundaries(perSampF & memSampF)
  fixBounds <- boundaries(eblInF|perSampF|sptF)
  fixStarts <- trialTimes[fixBounds[,1]]
  if(length(fixStarts)<stringsPerTrial){
    fixStarts <- c(fixStarts, seq(max(fixStarts)+300, maxTime-200, length.out=stringsPerTrial-length(fixStarts)))
  }
  animationLoop <- function(){
    for (i in 2:length(trialTimes)){
        setupPlot(accLabel, trialLabel, polParams, samplerParams, maxTime, traces$trialStrings, fixStarts)
        bounds <- truncateBounds(eblInBounds,i)
        if(nrow(bounds)>0) plotEBL(bounds)
        bounds <- truncateBounds(eblOutBounds,i)
        if(nrow(bounds)>0) plotEBL(bounds)
        bounds <- truncateBounds(perSamplingBounds,i)
        if(nrow(bounds)>0) plotPerSampling(bounds)
        bounds <- truncateBounds(sptBounds,i)
        if(nrow(bounds)>0) plotSacProg(bounds)
        bounds <- truncateBounds(setBounds,i)
        if(nrow(bounds)>0) plotSacExec(bounds)
        if(nrow(memSamplingBounds)>0){
          bounds <- truncateBounds(memSamplingBounds,i)
          if(nrow(bounds)>0) plotMemSampling(bounds)
        }
        if(nrow(droppedSampBounds)>0){
          bounds <- truncateBounds(droppedSampBounds, i)
          if(nrow(bounds)>0) plotDroppedSamples(bounds)
        }
        bounds <- truncateBounds(fixBounds, i)
        if(nrow(bounds)>0) plotFixations(bounds)
        plotPositionBeliefs(positionBeliefs[1:i,], perSampF[1:i]|sptF[1:i]|eblOutF[1:i], memSampF[1:i], trialTimes[1:i])
        plotTrialBeliefs(trialBeliefs[1:i], perSampF[1:i]|sptF[1:i]|eblOutF[1:i] , memSampF[1:i], trialTimes[1:i])
        if(motorStart<=i){
          plotMotor(c(motorStart,i), trialTimes[1:i])
        }
        draw.circle(x=fixStarts[focus[i]+1]+-10+rnorm(1,sd=15), y=3.95+rnorm(1,sd=0.02), radius=50, col="#00ff0055", border="#00ff0055")
    }
    text(label=responseLabel,cex=1.8,x=maxTime-500,y = 1.2, pos=4,col="red")
  }
  if(type=="HTML"){
    saveHTML(animationLoop(), ani.width=800, ani.height=600, interval=0.05, description="Model Simulation (half speed)", verbose=F, img.dir=name, img.name=sprintf("%s_plot", name), htmlfile=sprintf("%s.html", name), outdir=outputDir)
  } else if(type=="SWF"){
    saveSWF(animationLoop(), ani.width=800, ani.height=600, interval=0.05, description="Model Simulation (half speed)", verbose=F, swf.name=sprintf("%s.swf",name), img.name=sprintf("%s_plot", name), outdir=outputDir)
  } else if(type=="MP4"){
    saveVideo(animationLoop(), ani.width=800, ani.height=600, interval=0.05, description="Model Simulation (half speed)", verbose=F, video.name=sprintf("%s.mp4",name), img.name=sprintf("%s_plot", name), clean=TRUE, outdir=outputDir)
  } else if(type=="MPG"){
    saveMovie(animationLoop(), ani.width=800, ani.height=600, interval=0.05, description="Model Simulation (half speed)", verbose=F, movie.name=sprintf("%s.mpg",name), img.name=sprintf("%s_plot", name), clean=TRUE, outdir=outputDir)
  } else if(type=="GIF"){
    saveMovie(animationLoop(), ani.width=800, ani.height=600, interval=0.05, description="Model Simulation (half speed)", verbose=F, movie.name=sprintf("%s.gif",name), img.name=sprintf("%s_plot", name), clean=TRUE, outdir=outputDir)
  }
  else{
    stop("Unknown Animation Type! Supported: HTML, SWF, MP4, MPG, MOV, GIF")
  }
}

truncateBounds <- function(bound, i){
  starts <- bound[,1]
  ends <- bound[,2]
  starts <- starts[starts<=i]
  ends <- ends[ends<=i]
  if(length(ends)<length(starts)){
    ends <- c(ends, i)
  }
  return(cbind(starts, ends))
}


plotFixations <- function(fixBounds){
  fix.top <- 3.5
  fix.bot <- fix.top - 0.5
  fix.col <- rgb(0.5,0.5,0.2,0.4)

  for (fix in 1:nrow(fixBounds)) {
    s <- fixBounds[fix,1]
    e <- fixBounds[fix,2]
    rect(s, fix.bot, e, fix.top,col=fix.col,border=NA)
    lines(x=c(s,s),y=c(fix.top,fix.top-3),col="black",lty=3, lwd=1)
    lines(x=c(e,e),y=c(fix.bot,fix.top-3),col="black",lty=3, lwd=1)
    #    text(label="FIXATION", cex=0.8, x=s + (e-s)/2, y = fix.bot + (fix.top-fix.bot)/2)
  }
  text(label="FIXATION", srt=90, cex=0.8, x=-50, y = fix.bot + (fix.top-fix.bot)/2)

  #      ## Put up non-fixated words
  #      if (nrow(fixBounds) < .stringsPerTrial) {
  #        x <- s + (e-s)/2
  #        for (j in (nrow(fixBounds)+1):.stringsPerTrial) {
  #          x <- x + 300
  #          text(label=traces$trialStrings[j],pos=3,
  #               x=x, y = fix.top + 0.3,cex=2)
  #        }
  #      }
}

plotSacProg <- function(sacProgBounds){
  sp.top <- 2.9
  sp.bot <- sp.top - 0.3
  sp.col <- rgb(0.3,0.4,0.2,0.3)
  text(label="SACC",srt=90, cex=0.8, x=-5, y = sp.bot + (sp.top-sp.bot)/2)
  if (nrow(sacProgBounds)==0) return()
  ## Saccade programs
  for (sp in 1:nrow(sacProgBounds)) {
    s <- sacProgBounds[sp,1]
    e <- sacProgBounds[sp,2]
    rect(s, sp.bot, e, sp.top,col=sp.col,border=NA)
    # lines(x=c(s,s),y=c(0,sp.top),col="black",lty=3, lwd=1)
  }
}

plotSacExec <- function(sacExecBounds){
  se.top <- 2.9
  se.bot <- se.top - 0.3
  se.col <- rgb(0.3,0.4,0.2,0.3)
  text(label="SACC",srt=90, cex=0.8, x=-5, y = se.bot + (se.top-se.bot)/2)
  if (nrow(sacExecBounds)==0) return()
  ## Saccade exec
  for (se in 1:nrow(sacExecBounds)) {
    s <- sacExecBounds[se,1]
    e <- sacExecBounds[se,2]
    rect(s, se.bot, e, se.top,col=se.col,border=NA)
    # lines(x=c(s,s),y=c(0,se.top),col="black",lty=3, lwd=1)
  }
}

plotEBL <- function(eblBounds){
  ebl.top <- 2.6
  ebl.bot <- ebl.top - 0.3
  ebl.col <- rgb(0,0,0,0.3)
  text(label="EBL", srt=90,cex=0.6, x=-50, y = ebl.bot + (ebl.top-ebl.bot)/2)
  if (nrow(eblBounds)==0) return()
  ## EBLs
  for (ebl in 1:nrow(eblBounds)) {
    s <- eblBounds[ebl,1]
    e <- eblBounds[ebl,2]
    rect(s, ebl.bot, e, ebl.top,col=ebl.col,border=NA)
    #    text(label="EBL", cex=0.6,srt=90, x=s + (e-s)/2, y = ebl.bot + (ebl.top-ebl.bot)/2)

  }

}

plotDroppedSamples <- function(sampOverlaps){
  dropped.samp.top <- 2.3
  dropped.samp.bot <- 2.0
  dropped.samp.col <- "darkred"
  if (nrow(sampOverlaps)==0) return()
  ## Perceptual Sampling
  for (dropped.samp in 1:nrow(sampOverlaps)) {
    s <- sampOverlaps[dropped.samp,1]
    e <- sampOverlaps[dropped.samp,2]
    rect(s, dropped.samp.bot, e, dropped.samp.top,col=dropped.samp.col,border=NA)
  }
}

plotPerSampling <- function(perSamplingBounds){
  per.samp.top <- 2.3
  per.samp.bot <- per.samp.top - 0.3
  per.samp.col <- rgb(0.3,0.4,0.2,0.3)
  if (nrow(perSamplingBounds) ==0) return()
  text(label="PER\nSAMP",srt=90, cex=0.8, x=-5, y = per.samp.bot + (per.samp.top-per.samp.bot)/2)
  ## Perceptual Sampling
  for (per.samp in 1:nrow(perSamplingBounds)) {
    s <- perSamplingBounds[per.samp,1]
    e <- perSamplingBounds[per.samp,2]
    rect(s, per.samp.bot, e, per.samp.top,col=per.samp.col,border=NA)
  }
  # lines(x=c(s,s),y=c(per.samp.top,0),lty=3, lwd=1)

}

plotMemSampling <- function(memSamplingBounds){
  mem.samp.top <- 1.9
  mem.samp.bot <- mem.samp.top - 0.3
  mem.samp.col <- rgb(0.3,0.4,0.2,0.5)
  text(label="MEM\nSAMP", srt=90,cex=0.8, x=-5, y = mem.samp.bot + (mem.samp.top-mem.samp.bot)/2)
  if (nrow(memSamplingBounds)==0) return()
  for (mem.samp in 1:nrow(memSamplingBounds)) {
    s <- memSamplingBounds[mem.samp,1]
    e <- memSamplingBounds[mem.samp,2]
    rect(s, mem.samp.bot, e, mem.samp.top,col=mem.samp.col,border=NA)
  }


}

plotMotor <- function(motorBounds){
  motor.top <- 1.5
  motor.bot <- motor.top - 0.5
  motor.col <- rgb(0.5,0.5,0.2,0.4)
  s <- motorBounds[1]
  e <- motorBounds[2]
  rect(s, motor.bot, e, motor.top, col=motor.col,border=NA)

  lines(x=c(e,e),y=c(motor.top,0),lty=1,lwd=4)
  text(label="MOTOR",cex=0.8,
       x=s + (e-s)/2, y = motor.bot + (motor.top - motor.bot )/2)

}

setupPlot <- function(accLabel, trialLabel, polParams, samplerParams, maxTime, trialStrings, fixStarts, sampStarts, sampEnds, stringsPerTrial=.stringsPerTrial){
  plot(NA, type="p",
       cex=0.4, pch=20,
       lwd=2,
       bty="n",
       yaxt="n",
       ylim=c(0,4.1),
       xlim=c(0,maxTime),
       ylab=NA,
       xlab="Trial Time (ms)",
       main=sprintf("%s %s Trial",accLabel,trialLabel),
       sub =
         sprintf("sacA=%.5f, sacB=%.5f, taskYes= %.5f, taskNo= %.5f, pNoise=%.2f, mNoise=%.2f", polParams$saccadeA, polParams$saccdeB, polParams$taskYes, polParams$taskNo, samplerParams$perceptNoise, samplerParams$memNoise))
  ## Backdrop for posterior
  rect(0,0,maxTime,1,col="grey90",border=NA)
  rect(sampStarts,0,sampEnds,1,col="grey70",border=NA)
  ## Threshold lines
  lines(x=c(0,maxTime),y=c(polParams$saccadeA,polParams$saccadeA),lty=2)
  lines(x=c(0,maxTime),y=c(polParams$taskYes,polParams$taskYes),lty=1)
  lines(x=c(0,maxTime),y=c(polParams$taskNo,polParams$taskNo),lty=1)
  text(label=trialStrings,pos=3, x=fixStarts, y = 3.8,cex=2)
}

plotPositionBeliefs <- function(positionBeliefs, allSampTimes){
  perMask <- allSampTimes$type=="Perception"
  memMask <- allSampTimes$type=="Memory"
  times <- allSampTimes$times

  for (p in 1:nrow(positionBeliefs)){
    # normal sampling
    points(positionBeliefs[p,perMask] ~ times[perMask],
           cex=0.2, pch=20, lwd=1,
           col=rainbow(.stringsPerTrial)[p],
           lty=1)
    # memory sampling
    points(positionBeliefs[p,memMask] ~ times[memMask], type="p",
           cex=0.2, pch=20, lwd=1,
           col=c(rainbow(.stringsPerTrial)[p], alpha=.5),
           lty=1)
    # now connect them by faint lines, incl. bridging gaps
    lines(positionBeliefs[p,] ~ times, type="l",
          cex=0.5, lwd=0.5,
          col=c(rainbow(.stringsPerTrial)[p], alpha=.5),
          lty=1)
  }
}

plotTrialBeliefs <- function(trialBeliefs, allSampTimes){
  perMask <- allSampTimes$type=="Perception"
  memMask <- allSampTimes$type=="Memory"
  times <- allSampTimes$times
  points(trialBeliefs[perMask] ~ times[perMask],
         cex=0.2, pch=20,
         lwd=1.5,
         ylim=c(0,1))

  points(trialBeliefs[memMask] ~ times[memMask], type="p",
         cex=0.2, pch=20,
         lwd=1.5,col="darkgray",
         ylim=c(0,1))
  # connect by pretty lines
  lines(trialBeliefs ~ times,
         cex=0.2, pch=20,
         lwd=0.5 ,col="darkgray",
         ylim=c(0,1))
}
