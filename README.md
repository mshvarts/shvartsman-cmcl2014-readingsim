# README #

This contains code used to simulate: 

Shvartsman, M., Lewis, R. L., and Singh, S. (2014). Computationally Rational Saccadic Control: An Explanation of Spillover Effects Based on Sampling from Noisy Perception and Memory. Paper presented at the 5th annual workshop on Cognitive Modeling and Computational Linguistics. 
Shvartsman, M., Lewis, R. L., & Singh, S. (2014) Spillover frequency effects in a sequential sampling model of reading. Talk given at the 27th annual CUNY conference on human sentence processing. 

It can also be used to simulate Lewis, R. L., Shvartsman, M., & Singh, S. (2013). The adaptive nature of eye movements in linguistic tasks: how payoff and architecture shape speed-accuracy trade-offs. Topics in Cognitive Science, 5(3), 581–610. *This is not the code actually used for that paper; rather, it is a code refactor that should produce identical results*. 

## Getting started ##
The code can be run as an R package, or a static binary. To set it up as an R package, you will need its R pacakge dependencies (Rcpp,plotrix, animation, plyr), plus the GNU scientific library (GSL). To set it up as a static binary, you just need GSL and gcc. 

###Please let me know if you want to use this -- I would love to help, and this would encourage me to write more documentation. I had an undergrad work with this codebase to do a senior thesis and it's written to be mostly grokkable and semi-self-documenting. ###

## Installation guides for R package ##
- Linux: install r-base-core and r-base-dev and libgsl (or equivalent if you're not ubuntu)
- OSX: install GSL. My favorite way is to install Homebrew (http://brew.sh/) and then `brew install gsl`. But you can do it from macports or fink or source. I use `gsl-config` to find it so as long as that's in the path, everything should work. Then, from the top level directory `CMD BUILD readingSim && R CMD INSTALL readingSim`. 
- Windows: Install RTools. Get the msys package from the mingw64 project (not vanilla msys) and stick it in /Rtools. Download GSL source. Open msys and navigate to gsl source dir.  `CPPFLAGS="-m64" LDFLAGS="-m64" ./configure`,  then `make && make install`. That should get you a GSL that will play nice with the R stuff. Then make sure C:\Rtools\msys\local\bin is in your PATH. Then `R CMD INSTALL --build readingSim`. 

## Installation guides for static binary ##
All platforms: you need gcc, and you need GSL installed in your path such that `gsl-config` works (as above). Then go into `static` and run `make clean && make`. You will get a binary in `/bin/$(KERNEL)/` that takes parameters in flat JSON and spits out results in the format that MindModeling@Home expects for aggregation. `parseJson()` in `/static/json.cpp` tells you all the options you can pass this way. `make cluster` compiles a static binary, including bundling the whole lexicon into the binary. This takes at least a few minutes. 

## Replicating the results: ##
The relevant papers should have all the parameter combinations used. You will need access to MindModeling@Home (https://mindmodeling.org/) or another cluster resource to run these. You will need to rewrite the interface to fit your cluster if you don't use MM@H. 

To just play around with things, the C++ simulation classes are all exposed as R classes via Rcpp. They are `Architecture, Belief, Experiment, Payoff, Sampler, Policy, Trial`, and `Recorder`. Not every method is exposed but I did my best to make key pieces available. There is a top-level wrapper called `simulateTrial()` that will talk to the C++ code and generate a single trial trace. It can also cherrypick arbitrary trial types (e.g. a correct word trial), though this may take very long with unusual trial requirements because it just runs trials randomly until it finds one that matches your requirements (i.e. "correct nonword trial" is easier to find than "incorrect trial with a nonword in 4th position and RT between 1900ms and 2300ms", though both are valid requirements). `staticPlot()` takes in the output of simulateTrial and generates traces.

## License ##
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). This means you are free to share or build on the work, but must give appropriate credit, provide a link to the license, and indicate if changes were made, and must distribute your contributions under the same license. 