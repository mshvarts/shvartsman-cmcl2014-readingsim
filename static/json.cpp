#include "json.h"


void Tokenize(const std::string& str, std::vector<std::string>& tokens, const std::string& delimiters){
//http://www.oopweb.com/CPP/Documents/CPPHOWTO/Volume/C++Programming-HOWTO-7.html
    // Skip delimiters at beginning.
    std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    std::string::size_type pos = str.find_first_of(delimiters, lastPos);

    while (std::string::npos != pos || std::string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}

bool charsToRemove(char c){
  if (c==' ') return true;
  if (c=='}') return true;
  if (c=='{') return true;
  if (c=='"') return true;
  return false;
}

void parseJson(std::string json, double &sacA, double &sacB, double &memA, double &memB, int &sampDeadline, double & taskYes, double & taskNo, double & pNoise, double & mNoise, double & uNoise, int & nTrials, double & spt, bool & perPrim){
    std::vector<std::string> commandLineParams;
    std::vector<std::string> onePar;
    std::string var;
    double val;
    // strip whitespace first
    json.erase(std::remove_if(json.begin(), json.end(), charsToRemove), json.end());
    Tokenize(json, commandLineParams,",");

    for(unsigned i=0; i < commandLineParams.size(); i++){
        Tokenize(commandLineParams[i], onePar, ":");
    }
    for(unsigned i=0; i <onePar.size(); i+=2){
        // if(onePar[i].c_str()[0]!='"'){
        //     std::runtime_error("Bad Json (or most likely this json parser is terrible). Write a better parser! Or make sure your json is flat and has no whitespace");
        // }
        // var.assign(onePar[i].substr(1,onePar[i].size()-2));
        var.assign(onePar[i]);
        val = (double)atof(onePar[i+1].c_str());
        if(var.compare("sacA")==0) {// 0 if equal
          sacA = val;
          continue;
        } else if(var.compare("sacB")==0) {// 0 if equal
          sacB = val;
          continue;
        } else if(var.compare("memA")==0) {// 0 if equal
          memA = val;
          continue;
        } else if(var.compare("memB")==0) {// 0 if equal
          memB = val;
          continue;
        } else if(var.compare("sampDeadline")==0) {// 0 if equal
          sampDeadline = val;
          continue;
        } else if(var.compare("taskYes")==0) {// 0 if equal
          taskYes = val;
          continue;
        } else if(var.compare("taskNo")==0) {// 0 if equal
          taskNo = val;
          continue;
          } else if(var.compare("nTrials")==0) {// 0 if equal
          nTrials = val;
          continue;
        } else if(var.compare("pNoise") == 0){
          pNoise = val;
          continue;
        } else if(var.compare("mNoise") == 0){
          mNoise = val;
          continue;
          } else if(var.compare("uNoise") == 0){
          uNoise = val;
          continue;
          } else if(var.compare("perceptPrim") == 0){
          perPrim = val == 1;
          continue;
          } else if(var.compare("spt") == 0){
          spt = val;
          continue;
          } else if(var.compare("nRuns") == 0){
          // transparent to the model, this is to make the cluster repeat points
          continue;
        } else {
            throw std::runtime_error("Unknown input parameter or malformed JSON!");
        }
    }
}

#ifndef IGNORE_RCPP
using namespace Rcpp;

// [[Rcpp::export]]
CharacterVector tokenizeTest(SEXP string, SEXP delimiter){
  std::string s = as<std::string>(string);
  std::string delim = as<std::string>(delimiter);
  std::vector<std::string> tokens;
  Tokenize(s, tokens, delim);
  CharacterVector out(tokens.size());
  for (unsigned i=0; i<tokens.size(); i++){
    out[i] = tokens[i];
  }
  return(out);
}

// [[Rcpp::export]]
List parseTest(SEXP string){
  std::string s = as<std::string>(string);
  double sacA, sacB, memA, memB, taskYes, taskNo;
  int nTrials;
  parseJson(s, sacA, sacB, memA, memB, taskYes, taskNo, nTrials);
  return List::create(Named("sacA")=sacA, Named("sacB")=sacB, Named("memA")=memA, Named("memB")=memB, Named("taskYes")=taskYes, Named("taskNo")=taskNo, Named("nTrials")=nTrials);
}
// [[Rcpp::export]]
std::string charEraser(std::string inputString){
   inputString.erase(std::remove_if(inputString.begin(), inputString.end(), charsToRemove), inputString.end());
 return inputString;
}



#endif
