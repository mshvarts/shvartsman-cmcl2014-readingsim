// include guard
#ifndef JSON_H
#define JSON_H

// no forward declarations needed

// includes we need
#ifndef IGNORE_RCPP
    #include <Rcpp.h>
#endif
#include <vector>
#include <iostream>
#include <stdexcept>
#include <stdlib.h> // for atof() and friends
#include <string>
#include <algorithm> // for remove_if and so on

void Tokenize(const std::string& str, std::vector<std::string>& tokens, const std::string& delimiters);
void parseJson(std::string json, double &sacA, double &sacB, double &memA, double &memB, int &sampDeadline, double & taskYes, double & taskNo, double & pNoise, double & mNoise, double & uNoise, int & nTrials, double & spt, bool & perPrim);

bool charsToRemove(char c);

#ifndef IGNORE_RCPP
using namespace Rcpp;
    CharacterVector tokenizeTest(SEXP string, SEXP delimiter);
    List parseTest(SEXP string);
    std::string charEraser(std::string inputString);
#endif

#endif
