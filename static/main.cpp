#include "../src/experiment.h"
#include "../src/constants.h"
#include "../src/payoff.h"
#include "../src/lexicon.h"
#include <iostream>
#include <fstream>
#include <stdexcept>
#include "../static/json.h" // eventually this can be a full-on json parser for mindmodeling. But for now, is easier to just do a simple strsplit

// this is stuff that we don't define based on std in input.
// TODO: separate this into boilerplate helper code that's the
// same for different run configurations and config-dependent code.

const int WORDFILE_LINES = 1500;
const int NONWORDFILE_LINES = 10366;
const std::string WORDFILE_NAME = "../../data/kf_words.txt";
const std::string NONWORDFILE_NAME = "../../data/arc_nonwords.txt";
// const int WORDFILE_LINES = 500;
// const int NONWORDFILE_LINES = 500;
// const std::string WORDFILE_NAME = "../../data/wordsAndFreqsArtificial.txt";
// const std::string NONWORDFILE_NAME = "../../data/nonwordsArtificial.txt";
// const int WORDFILE_LINES = 234;
// const int NONWORDFILE_LINES = 52;
// const std::string WORDFILE_NAME = "../../data/exp_words.txt";
// const std::string NONWORDFILE_NAME = "../../data/exp_nonwords.txt";
const int NTRIALS = 10;
const double EBL = 50;
const double SPT = 100;
const double SET = 40;
const double MOTORTIME = 250;
const double COEFVAR = 0.3;
// don't set these to zero, set to something very low (otherwise things get funny)
// const double EBL = 0.0001;
// const double SPT = 0.0001;
// const double SET = 0.0001;
// const double MOTORTIME = 0;
// const double COEFVAR = 0.0001;
const double PERCEPTPRIM = false;
const double PNOISE = 1.2;
const double MNOISE = 1.2;
const double UNOISE = 0;
const double SACA = 0;
const double SACB = 0;
const double MEMA = 0;
const double MEMB = 0;
const double TASKYES = 0;
const double TASKNO = 0;
const double TRIALWORDPRIOR = 0.5;
const double PROBWORDTRIAL = 0.5;
const double MAXTRIALDUR = 20000;
const double CONDITIONINGPOST = 1;
const std::string PLABELS[N_PAYOFFS] = {"SPEED", "BAL", "ACC"};

void dumpFail(std::string in){
  std::cout << "IVs=" << in << std::endl;
  std::cout << "payoff_SPEED_mean=-1\n payoff_SPEED_var=-1\n payoff_SPEED_n=-1\n payoff_BAL_mean=-1\n payoff_BAL_var=-1\n payoff_BAL_n=-1\n payoff_ACC_mean=-1\n payoff_ACC_var=-1\n payoff_ACC_n=-1\n RT_mean=-1\n RT_var=-1\n RT_n=-1\n wordRT_mean=-1\n wordRT_var=-1\n wordRT_n=-1\n nonwordRT_mean=-1\n nonwordRT_var=-1\n nonwordRT_n=-1\n Acc_mean=-1\n Acc_var=-1\n Acc_n=-1\n wordAcc_mean=-1\n wordAcc_var=-1\n wordAcc_n=-1\n nonwordAcc_mean=-1\n nonwordAcc_var=-1\n nonwordAcc_n=-1\n SFD_mean=-1\n SFD_var=-1\n SFD_n=-1\n wordSFD_mean=-1\n wordSFD_var=-1\n wordSFD_n=-1\n nonwordSFD_mean=-1\n nonwordSFD_var=-1\n nonwordSFD_n=-1\n lowFreqSFD_mean=-1\n lowFreqSFD_var=-1\n lowFreqSFD_n=-1\n highFreqSFD_mean=-1\n highFreqSFD_var=-1\n highFreqSFD_n=-1\n lowPrevFreqSFD_mean=-1\n lowPrevFreqSFD_var=-1\n lowPrevFreqSFD_n=-1\n highPrevFreqSFD_mean=-1\n highPrevFreqSFD_var=-1\n highPrevFreqSFD_n=-1\n SFD_word_0_mean=-1\n SFD_word_0_var=-1\n SFD_word_0_n=-1\n SFD_word_1_mean=-1\n SFD_word_1_var=-1\n SFD_word_1_n=-1\n SFD_word_2_mean=-1\n SFD_word_2_var=-1\n SFD_word_2_n=-1\n SFD_word_3_mean=-1\n SFD_word_3_var=-1\n SFD_word_3_n=-1\n SFD_word_4_mean=-1\n SFD_word_4_var=-1\n SFD_word_4_n=-1\n SFD_word_5_mean=-1\n SFD_word_5_var=-1\n SFD_word_5_n=-1\n stringsRead_mean=-1\n stringsRead_var=-1\n stringsRead_n=-1\n wordTrialStringsRead_mean=-1\n wordTrialStringsRead_var=-1\n wordTrialStringsRead_n=-1\n nonwordTrialStringsRead_mean=-1\n nonwordTrialStringsRead_var=-1\n nonwordTrialStringsRead_n=-1\n correctTrialStringsRead_mean=-1\n correctTrialStringsRead_var=-1\n correctTrialStringsRead_n=-1\n incorrectTrialStringsRead_mean=-1\n incorrectTrialStringsRead_var=-1\n incorrectTrialStringsRead_n=-1\n sampsToDecision_mean=-1\n sampsToDecision_var=-1\n sampsToDecision_n=-1\n perSampCount_mean=-1\n perSampCount_var=-1\n perSampCount_n=-1\n memSampCount_mean=-1\n memSampCount_var=-1\n memSampCount_n=-1\n droppedSampCount_mean=-1\n droppedSampCount_var=-1\n droppedSampCount_n=-1\n corrTrialSFD_mean=-1\n corrTrialSFD_var=-1\n corrTrialSFD_n=-1\n incorrTrialSFD_mean=-1\n incorrTrialSFD_var=-1\n incorrTrialSFD_n=-1\n corrLowFreqSFD_mean=-1\n corrLowFreqSFD_var=-1\n corrLowFreqSFD_n=-1\n corrHighFreqSFD_mean=-1\n corrHighFreqSFD_var=-1\n corrHighFreqSFD_n=-1\n incorrLowFreqSFD_mean=-1\n incorrLowFreqSFD_var=-1\n incorrLowFreqSFD_n=-1\n incorrHighFreqSFD_mean=-1\n incorrHighFreqSFD_var=-1\n incorrHighFreqSFD_n=-1\n sampsToDecisionLowFreq_mean=-1\n sampsToDecisionLowFreq_var=-1\n sampsToDecisionLowFreq_n=-1\n sampsToDecisionHighFreq_mean=-1\n sampsToDecisionHighFreq_var=-1\n sampsToDecisionHighFreq_n=-1\n lowNeighDensSFD_mean=-1\n lowNeighDensSFD_var=-1\n lowNeighDensSFD_n=-1\n highNeighDensSFD_mean=-1\n highNeighDensSFD_var=-1\n highNeighDensSFD_n=-1\n" << std::endl;
}


//void dumpResults(double sacA, double sacB, double memA, double memB,double taskYes,double taskNo, int nTrials, double pNoise, double mNoise, double uNoise, Recorder *rec, std::string in){
void dumpResults(Recorder *rec, std::string in){
  // the ugly ugly price we pay for having sensible, modular, human-readable estimate containers elsewhere

  //// parameters
  // std::cout<<"IVs={"<< "\"sacA\":"<<sacA <<",\"sacB\":"<< sacB <<",\"memA\":"<< memA <<",\"memB\":"<< memB<<",\"taskYes\":"<< taskYes <<",\"taskNo\":"<<taskNo<<",\"nTrials\":"<<nTrials <<"\"pNoise\":"<< pNoise <<"\"mNoise\":"<<mNoise<<"\"uNoise\":"<<uNoise<<"}" << std::endl;
  std::cout << "IVs=" << in << std::endl;
  //// payoffs
  for (int i=0; i<N_PAYOFFS; i++){
      std::cout << "payoff_" << PLABELS[i] << "_mean" << "=" <<rec->payoffEstimate[i][0]<<std::endl;
      std::cout << "payoff_" << PLABELS[i] << "_var" << "=" <<rec->payoffEstimate[i][1]<<std::endl;
      std::cout << "payoff_" << PLABELS[i] << "_n" << "=" <<rec->payoffEstimate[i][2]<<std::endl;
  }
  ///// RT measurements:
  // global RT
  std::cout << "RT_mean="<<rec->globRT[0] << std::endl;
  std::cout << "RT_var="<<rec->globRT[1] << std::endl;
  std::cout << "RT_n="<<rec->globRT[2] << std::endl;
  // word RT
  std::cout << "wordRT_mean="<<rec->wordRT[0] << std::endl;
  std::cout << "wordRT_var="<<rec->wordRT[1] << std::endl;
  std::cout << "wordRT_n="<<rec->wordRT[2] << std::endl;
  // nonword RT
  std::cout << "nonwordRT_mean="<<rec->nonwordRT[0] << std::endl;
  std::cout << "nonwordRT_var="<<rec->nonwordRT[1] << std::endl;
  std::cout << "nonwordRT_n="<<rec->nonwordRT[2] << std::endl;
  ///// Accuracy measurements
  // global Acc
  std::cout << "Acc_mean="<<rec->globAcc[0] << std::endl;
  std::cout << "Acc_var="<<rec->globAcc[1] << std::endl;
  std::cout << "Acc_n="<<rec->globAcc[2] << std::endl;
  // word Acc
  std::cout << "wordAcc_mean="<<rec->wordAcc[0] << std::endl;
  std::cout << "wordAcc_var="<<rec->wordAcc[1] << std::endl;
  std::cout << "wordAcc_n="<<rec->wordAcc[2] << std::endl;
  // nonword Acc
  std::cout << "nonwordAcc_mean="<<rec->nonwordAcc[0] << std::endl;
  std::cout << "nonwordAcc_var="<<rec->nonwordAcc[1] << std::endl;
  std::cout << "nonwordAcc_n="<<rec->nonwordAcc[2] << std::endl;
  ///// SFD measurements:
  // Global SFD:
  std::cout << "SFD_mean="<<rec->globSFD[0] << std::endl;
  std::cout << "SFD_var="<<rec->globSFD[1] << std::endl;
  std::cout << "SFD_n="<<rec->globSFD[2] << std::endl;
  // Word SFD:
  std::cout << "wordSFD_mean="<<rec->wordSFD[0] << std::endl;
  std::cout << "wordSFD_var="<<rec->wordSFD[1] << std::endl;
  std::cout << "wordSFD_n="<<rec->wordSFD[2] << std::endl;
  // Nonword SFD
  std::cout << "nonwordSFD_mean="<<rec->nonwordSFD[0] << std::endl;
  std::cout << "nonwordSFD_var="<<rec->nonwordSFD[1] << std::endl;
  std::cout << "nonwordSFD_n="<<rec->nonwordSFD[2] << std::endl;
  // SFD by frequency: low and high
  std::cout << "lowFreqSFD_mean="<<rec->lowFreqSFD[0] << std::endl;
  std::cout << "lowFreqSFD_var="<<rec->lowFreqSFD[1] << std::endl;
  std::cout << "lowFreqSFD_n="<<rec->lowFreqSFD[2] << std::endl;
  std::cout << "highFreqSFD_mean="<<rec->highFreqSFD[0] << std::endl;
  std::cout << "highFreqSFD_var="<<rec->highFreqSFD[1] << std::endl;
  std::cout << "highFreqSFD_n="<<rec->highFreqSFD[2] << std::endl;
  // SFD by prevFreq: low and high
  std::cout << "lowPrevFreqSFD_mean="<<rec->lowPrevFreqSFD[0] << std::endl;
  std::cout << "lowPrevFreqSFD_var="<<rec->lowPrevFreqSFD[1] << std::endl;
  std::cout << "lowPrevFreqSFD_n="<<rec->lowPrevFreqSFD[2] << std::endl;
  std::cout << "highPrevFreqSFD_mean="<<rec->highPrevFreqSFD[0] << std::endl;
  std::cout << "highPrevFreqSFD_var="<<rec->highPrevFreqSFD[1] << std::endl;
  std::cout << "highPrevFreqSFD_n="<<rec->highPrevFreqSFD[2] << std::endl;
  // SFD by position
  for (int i = 0; i<STRINGS_PER_TRIAL; i++){
    std::cout << "SFD_word_"<< i <<"_mean="<<rec->posSFD[i][0] << std::endl;
    std::cout << "SFD_word_"<< i <<"_var="<<rec->posSFD[i][1] << std::endl;
    std::cout << "SFD_word_"<< i <<"_n="<<rec->posSFD[i][2] << std::endl;
  }
  ///// How many strings have we read?
  // global
  std::cout << "stringsRead_mean="<<rec->stringsRead[0] << std::endl;
  std::cout << "stringsRead_var="<<rec->stringsRead[1] << std::endl;
  std::cout << "stringsRead_n="<<rec->stringsRead[2] << std::endl;
  // word
  std::cout << "wordTrialStringsRead_mean="<<rec->wordTrialStringsRead[0] << std::endl;
  std::cout << "wordTrialStringsRead_var="<<rec->wordTrialStringsRead[1] << std::endl;
  std::cout << "wordTrialStringsRead_n="<<rec->wordTrialStringsRead[2] << std::endl;
  // nonword
  std::cout << "nonwordTrialStringsRead_mean="<<rec->nonwordTrialStringsRead[0] << std::endl;
  std::cout << "nonwordTrialStringsRead_var="<<rec->nonwordTrialStringsRead[1] << std::endl;
  std::cout << "nonwordTrialStringsRead_n="<<rec->nonwordTrialStringsRead[2] << std::endl;
  // correct
  std::cout << "correctTrialStringsRead_mean="<<rec->correctTrialStringsRead[0] << std::endl;
  std::cout << "correctTrialStringsRead_var="<<rec->correctTrialStringsRead[1] << std::endl;
  std::cout << "correctTrialStringsRead_n="<<rec->correctTrialStringsRead[2] << std::endl;
  // incorrect
  std::cout << "incorrectTrialStringsRead_mean="<<rec->incorrectTrialStringsRead[0] << std::endl;
  std::cout << "incorrectTrialStringsRead_var="<<rec->incorrectTrialStringsRead[1] << std::endl;
  std::cout << "incorrectTrialStringsRead_n="<<rec->incorrectTrialStringsRead[2] << std::endl;
  // sampsToDecision
  std::cout << "sampsToDecision_mean="<<rec->sampsToDecision[0] << std::endl;
  std::cout << "sampsToDecision_var="<<rec->sampsToDecision[1] << std::endl;
  std::cout << "sampsToDecision_n="<<rec->sampsToDecision[2] << std::endl;
  // perSampCount
  std::cout << "perSampCount_mean="<<rec->perSampCount[0] << std::endl;
  std::cout << "perSampCount_var="<<rec->perSampCount[1] << std::endl;
  std::cout << "perSampCount_n="<<rec->perSampCount[2] << std::endl;
  // memSampCount
  std::cout << "memSampCount_mean="<<rec->memSampCount[0] << std::endl;
  std::cout << "memSampCount_var="<<rec->memSampCount[1] << std::endl;
  std::cout << "memSampCount_n="<<rec->memSampCount[2] << std::endl;
  // droppedSampCount
  std::cout << "droppedSampCount_mean="<<rec->droppedSampCount[0] << std::endl;
  std::cout << "droppedSampCount_var="<<rec->droppedSampCount[1] << std::endl;
  std::cout << "droppedSampCount_n="<<rec->droppedSampCount[2] << std::endl;
  // corrTrialSFD
  std::cout << "corrTrialSFD_mean="<<rec->corrTrialSFD[0] << std::endl;
  std::cout << "corrTrialSFD_var="<<rec->corrTrialSFD[1] << std::endl;
  std::cout << "corrTrialSFD_n="<<rec->corrTrialSFD[2] << std::endl;
  // incorrTrialSFD
  std::cout << "incorrTrialSFD_mean="<<rec->incorrTrialSFD[0] << std::endl;
  std::cout << "incorrTrialSFD_var="<<rec->incorrTrialSFD[1] << std::endl;
  std::cout << "incorrTrialSFD_n="<<rec->incorrTrialSFD[2] << std::endl;
  // corrLowFreqSFD
  std::cout << "corrLowFreqSFD_mean="<<rec->corrLowFreqSFD[0] << std::endl;
  std::cout << "corrLowFreqSFD_var="<<rec->corrLowFreqSFD[1] << std::endl;
  std::cout << "corrLowFreqSFD_n="<<rec->corrLowFreqSFD[2] << std::endl;
  // corrHighFreqSFD
  std::cout << "corrHighFreqSFD_mean="<<rec->corrHighFreqSFD[0] << std::endl;
  std::cout << "corrHighFreqSFD_var="<<rec->corrHighFreqSFD[1] << std::endl;
  std::cout << "corrHighFreqSFD_n="<<rec->corrHighFreqSFD[2] << std::endl;
  // incorrLowFreqSFD
  std::cout << "incorrLowFreqSFD_mean="<<rec->incorrLowFreqSFD[0] << std::endl;
  std::cout << "incorrLowFreqSFD_var="<<rec->incorrLowFreqSFD[1] << std::endl;
  std::cout << "incorrLowFreqSFD_n="<<rec->incorrLowFreqSFD[2] << std::endl;
  // incorrHighFreqSFD
  std::cout << "incorrHighFreqSFD_mean="<<rec->incorrHighFreqSFD[0] << std::endl;
  std::cout << "incorrHighFreqSFD_var="<<rec->incorrHighFreqSFD[1] << std::endl;
  std::cout << "incorrHighFreqSFD_n="<<rec->incorrHighFreqSFD[2] << std::endl;
  // sampsToDecisionLowFreq
  std::cout << "sampsToDecisionLowFreq_mean="<<rec->sampsToDecisionLowFreq[0] << std::endl;
  std::cout << "sampsToDecisionLowFreq_var="<<rec->sampsToDecisionLowFreq[1] << std::endl;
  std::cout << "sampsToDecisionLowFreq_n="<<rec->sampsToDecisionLowFreq[2] << std::endl;
  // sampsToDecisionHighFreq
  std::cout << "sampsToDecisionHighFreq_mean="<<rec->sampsToDecisionHighFreq[0] << std::endl;
  std::cout << "sampsToDecisionHighFreq_var="<<rec->sampsToDecisionHighFreq[1] << std::endl;
  std::cout << "sampsToDecisionHighFreq_n="<<rec->sampsToDecisionHighFreq[2] << std::endl;
  // lowNeighDensSFD
  std::cout << "lowNeighDensSFD_mean="<<rec->lowNeighDensSFD[0] << std::endl;
  std::cout << "lowNeighDensSFD_var="<<rec->lowNeighDensSFD[1] << std::endl;
  std::cout << "lowNeighDensSFD_n="<<rec->lowNeighDensSFD[2] << std::endl;
  // highNeighDensSFD
  std::cout << "highNeighDensSFD_mean="<<rec->highNeighDensSFD[0] << std::endl;
  std::cout << "highNeighDensSFD_var="<<rec->highNeighDensSFD[1] << std::endl;
  std::cout << "highNeighDensSFD_n="<<rec->highNeighDensSFD[2] << std::endl;

  // for (unsigned i=0; i<rec->neighDensSFD.size(); i++){
  //   std::cout << "neighDensSFD"<<i<<"_mean="<<rec->neighDensSFD[i][0] << std::endl;
  //   std::cout << "neighDensSFD"<<i<<"_var="<<rec->neighDensSFD[i][1] << std::endl;
  //   std::cout << "neighDensSFD"<<i<<"_n="<<rec->neighDensSFD[i][2] << std::endl;
  // }
}



int main(int argc, const char *argv[]){
  std::string in;
  //double ebl=0, spt=0, set=0, motorTime=0, coefVar=0, perceptPrim=0, perceptNoise=0, memNoise=0, trialWordPrior=0, probWordTrial=0, maxTrialDuration=0, timePerStep=0, conditioningPost=0;
  double sacB=SACB, memB=MEMB, taskYes=TASKYES, taskNo=TASKNO, pNoise=PNOISE, mNoise=MNOISE, uNoise=UNOISE;
  double sacA = SACA;
  double memA = MEMA;
  bool perPrim = PERCEPTPRIM; 
  double spt = SPT; 
  int nTrials=NTRIALS;
  int sampDeadline = MAX_SAMPS; 
  std::string words[N_WORDS];
  std::string nonwords[N_NONWORDS];
  double freqs[N_WORDS];

  #ifdef STATIC_CLUSTER
    loadWordsNoDisk(words, freqs, WORDFILE_LINES);
    loadNonwordsNoDisk(nonwords, NONWORDFILE_LINES);
  #else
    loadWords(words, freqs, WORDFILE_NAME, WORDFILE_LINES);
    loadNonwords(nonwords, NONWORDFILE_NAME, NONWORDFILE_LINES);
  #endif
  int wNeighDens[N_WORDS];
  int nwNeighDens[N_NONWORDS];
   _computeLexNeighborhoodSize(words, nonwords, wNeighDens, nwNeighDens);
  Recorder * rec = new Recorder(freqs, wNeighDens, nwNeighDens);
  Experiment e(EBL, spt, SET, MOTORTIME, COEFVAR, perPrim, PNOISE, MNOISE, UNOISE, words, nonwords, freqs, TRIALWORDPRIOR, PROBWORDTRIAL, MAXTRIALDUR, TIMEPERSTEP, CONDITIONINGPOST);
  while(std::cin){
    getline(std::cin, in);
    if (in.empty()){
      std::cout << "Found empty input line, exiting!" << std::endl;
      return 0;
    }
    else {
      parseJson(in, sacA, sacB, memA, memB, sampDeadline, taskYes, taskNo, pNoise, mNoise, uNoise, nTrials, spt, perPrim);
      // if(memA != 0 && sacA > memA){ // if saccade thresh is above memthreh, just toss this combo, unless it's 0 (no-memory)
      //   dumpFail(in);
      //   return 0;
      // }
      e.setPerceptNoise(pNoise);
      e.setMemNoise(mNoise);
      e.setUpdateNoise(uNoise);
      e.setSPT(spt);
      e.setPerPrim(perPrim);
      if (taskYes == -1){ // signal for symmetric thresh
        taskYes = taskNo;
      }
      e.setPolicy(sacA, sacB, memA, memB, taskYes, taskNo, sampDeadline);
      //e.runTrials(NTRIALS, rec);
      e.runTrials(nTrials, rec);
      // dumpResults(sacA, sacB, memA, memB, taskYes, taskNo, nTrials, pNoise, mNoise, uNoise, rec, in);
      dumpResults(rec, in);
      rec->reset();
    }
  }
  delete [] rec;
}

#ifndef IGNORE_RCPP

void recorderDumpTest(){
  std::string words[N_WORDS];
  double freqs[N_WORDS];
  loadWords(words, freqs);
  Recorder * rec = new Recorder(freqs);
  rec->generateFakeRecords();
  dumpResults(10, 11, 12, 13, 14, 15, 17, rec);
  delete [] rec;
}


#endif
