// include guard
#ifndef PAYOFF_H
#define PAYOFF_H


// includes we need
#ifndef IGNORE_RCPP
	#include <Rcpp.h>
#endif

#include "constants.h"

// actual class
class Payoff{
	public:
		static double getPayoff(double rt, double acc, int payoffID); 
		//double getPayoff(double rt, double acc); 
		//Payoff(double *pSpeeds, double *pAccs, double *cutoffs, int nPayoffs); 
		//Payoff(double pSpeed, double pAcc, double cutoff); 
		
		// #ifndef IGNORE_RCPP
			// Payoff(SEXP params); 
			// getPayoff(double rt, double acc, int payoffID);
			// Rcpp::XPtr<Payoff> returnPointerForR();
		// #endif
	private:

		// double pSpeed; 
		// double pAcc; 
		// double cutoff; 
};

#endif
