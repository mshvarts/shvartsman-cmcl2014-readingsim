// include guard
#ifndef BELIEF_H
#define BELIEF_H

// no forward declarations needed

// includes we need
#include "constants.h" 
#include <gsl/gsl_randist.h> // need this for prob dens funcs
#include "rng.h" // for noisifying sample pre-update
#include <string>
#include <stdexcept>
#include "helpers.h" // currently just for sd of a sum of gaussians
#include <algorithm> // for max_element for getMaxPosBelief()

#ifndef IGNORE_RCPP
    #include <Rcpp.h>
#endif

class Belief{
    public:
        void bayesUpdate(double samp[CHARS_PER_STRING][ALPHABET_SIZE], int pos, double likelihoodNoise); 
        double getPositionBelief(int pos); 
        double getMaxPositionBelief(int pos);
        double getTrialBelief(); 
        double getConditioningBelief(int focus); 
        void reset(); 
        void updateCharPosDens(double samp[CHARS_PER_STRING][ALPHABET_SIZE], double noise);
        void populateStringCharDens();
        void calcStringPosteriors(int pos);
        void calcTrialPosterior(int pos);
        void setUpdateNoise(double updateNoise);
        ~Belief(); 
        Belief(std::string words[N_WORDS], std::string nonwords[N_NONWORDS], double wf[N_WORDS], double trialWordPrior, int conditioningPost, double updateNoise);
        #ifndef IGNORE_RCPP
            void bayesUpdateForR(Rcpp::NumericMatrix samp, int pos, double likelihoodNoise); 
            void updateCharPosDensForR(Rcpp::NumericMatrix samp, double noise);
            Rcpp::NumericMatrix wordBeliefGetterForR();
            Rcpp::NumericMatrix nonwordBeliefGetterForR();
            Rcpp::List wcdGetterForR();
            Rcpp::List ncdGetterForR();
            Rcpp::List cpdGetterForR();
            Rcpp::XPtr<Belief> returnPointerForR();
            Belief(SEXP words, SEXP nonwords, SEXP freqs, SEXP params); 
            Rcpp::NumericVector normalizeR(Rcpp::NumericVector v);
        #endif
        
    private: 
        void noisifySample(double samp[CHARS_PER_STRING][ALPHABET_SIZE]);
        static double wordBelief[STRINGS_PER_TRIAL][N_WORDS];
        static double nonwordBelief[STRINGS_PER_TRIAL][N_NONWORDS];
        static double trialBelief[STRINGS_PER_TRIAL+1]; 
        static double wordCharDensity[N_WORDS][CHARS_PER_STRING][ALPHABET_SIZE]; 
        static double nonwordCharDensity[N_NONWORDS][CHARS_PER_STRING][ALPHABET_SIZE]; 
        static double charPosDensity[CHARS_PER_STRING][ALPHABET_SIZE][2]; 
        static double wordFreqs[N_WORDS];
        double trialWordPrior; 
        long double kahan_sum(double *vec, int size);
        double nonwordStringProb;
        int wordCodes[N_WORDS][CHARS_PER_STRING][ALPHABET_SIZE];
        int nonwordCodes[N_NONWORDS][CHARS_PER_STRING][ALPHABET_SIZE];         
        int conditioningPost; 
        void normalize(double *belief, int size); 
        double _updateNoise; 
        bool _ownsRng; 

};

#endif



