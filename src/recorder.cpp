#include "recorder.h"
// #include <iostream>

Recorder::Recorder(double *wf, int *wnd, int *nwnd){	
	for (int i=0; i<N_WORDS; i++){
        wordFreqs[i] = wf[i];
        wordNeighDens[i] = wnd[i];
    }
    for (int i=0; i<N_NONWORDS; i++){
        nonwordNeighDens[i] = nwnd[i];
    }
    freqSplit = findFreqSplit(); 
    neighSplit = findNeighSplit(); 
    reset();
}

void Recorder::reset(){
	// int maxNeighDens = *std::max_element(wordNeighDens, wordNeighDens + N_WORDS);
	// neighDensSFD.resize(maxNeighDens+1); // max + 1 since 0 is possible
	// for (unsigned j=0; j<neighDensSFD.size(); j++){
	// 	neighDensSFD[j].resize(3);
	// 	for (int i = 0; i<3; i++){
	// 		neighDensSFD[j][i] = 0; 
	// 	}
	// }
	for (int i=0; i<3; i++){
		globRT[i]=0;
		globAcc[i]=0;
		globSFD[i]=0; 

		lowFreqSFD[i]=0; 
		highFreqSFD[i]=0; 

		perSampCount[i] = 0;
		memSampCount[i] = 0;
		droppedSampCount[i] = 0;

		lowPrevFreqSFD[i]=0;
		highPrevFreqSFD[i]=0; 

		corrTrialSFD[i]=0;
		incorrTrialSFD[i]=0;

		corrLowFreqSFD[i]=0; 
		corrHighFreqSFD[i]=0; 
		incorrLowFreqSFD[i]=0; 
		incorrHighFreqSFD[i]=0; 

		sampsToDecision[i] = 0;
		sampsToDecisionLowFreq[i]=0;
		sampsToDecisionHighFreq[i]=0;

		wordRT[i]=0;
		nonwordRT[i]=0;
		wordSFD[i]=0;
		nonwordSFD[i]=0;
		
		wordAcc[i]=0;
		nonwordAcc[i]=0;

		stringsRead[i] = 0;
		wordTrialStringsRead[i] = 0;
		nonwordTrialStringsRead[i] = 0;
		correctTrialStringsRead[i] = 0;
		incorrectTrialStringsRead[i] = 0;

		lowNeighDensSFD[i] = 0;
		highNeighDensSFD[i] = 0;
		
		for (int j = 0; j < STRINGS_PER_TRIAL; j++){
			posSFD[j][i] = 0;
		}
		for (int j = 0; j < N_PAYOFFS; j++){
			payoffEstimate[j][i]= 0; 
		}
	}
}

double Recorder::_findMedian(vector<double> freqs) {
// modified from http://stackoverflow.com/questions/1719070/what-is-the-right-approach-when-using-stl-container-for-median-calculation
  std::size_t middleIdx = N_WORDS/2;
  std::nth_element(freqs.begin(), freqs.begin()+middleIdx, freqs.end());
  if(N_WORDS % 2 != 0){ //Odd number of elements
    return freqs[middleIdx];
  }else{            //Even number of elements
    double medianLeft = freqs[middleIdx];
    double medianRight = *std::max_element(freqs.begin(), freqs.begin()+middleIdx); 
    // std::cout << (medianLeft+medianRight)/2.0 << "ARGH" << std::endl; 
    return (medianLeft+medianRight)/2.0;
  }
}

double Recorder::findFreqSplit(){
	vector<double> myFreqs(wordFreqs, wordFreqs+N_WORDS); // copy the freqs vector because findMedian() unpredictably sorts it
	return _findMedian(myFreqs); 
}

double Recorder::findNeighSplit(){
	// copy the neighs vectors because findMedian will unpredictably sort
	vector<double> wDens(wordNeighDens, wordNeighDens+N_WORDS); 
	vector<double> nwDens(nonwordNeighDens, nonwordNeighDens+N_NONWORDS); 
	wDens.insert(wDens.end(), nwDens.begin(), nwDens.end());
	return _findMedian(wDens); 
}

void Recorder::savePerSamps(int count){
	updateEstimate(count, perSampCount);
}
void Recorder::saveMemSamps(int count){
	updateEstimate(count, memSampCount);
}
void Recorder::saveDroppedSamps(int count){
	updateEstimate(count, droppedSampCount);
}

void Recorder::saveSampsToDecision(int count, int focus){
	updateEstimate(count, sampsToDecision);
	if(drawnTrialWords[focus] == -1) return; 
	if (wordFreqs[drawnTrialWords[focus]] > freqSplit){
		updateEstimate(count, sampsToDecisionHighFreq);
	} else{
		updateEstimate(count, sampsToDecisionLowFreq);
	}
}

void Recorder::recordFixationEnd(double sfd, int pos){
	// std::cout << "Storing SFD"<<sfd <<" at pos" << pos << std::endl; 
	_sfdTemp[pos] = sfd; 
}

void Recorder::setTrialInfo(int tw[STRINGS_PER_TRIAL], int tt){
	for (int i =0; i<STRINGS_PER_TRIAL; i++){
		drawnTrialWords[i] = tw[i];
	}
	trialType = tt;
}

void Recorder::recordTrialEnd(double RT, int response, int focus, int nFix){
	updateEstimate(double(focus), stringsRead);
	double p = 0; 
	double acc = response==trialType? 1 : 0; 
	updateEstimate(RT, globRT);
	updateEstimate(acc, globAcc);
	// if we ran out of strings, record this as a response on the final string. 
	focus = focus == STRINGS_PER_TRIAL ? focus-1 : focus; 
	if (trialType == 1){// word
		updateEstimate((double)focus, wordTrialStringsRead);
		updateEstimate(RT, wordRT);
		updateEstimate(acc, wordAcc);
	} else {
		updateEstimate((double)focus, nonwordTrialStringsRead);
		updateEstimate(RT, nonwordRT);
		updateEstimate(acc, nonwordAcc);
	}
	if (acc==1){
		updateEstimate((double)focus, correctTrialStringsRead);
		for (int i=0; i<=focus; i++){
			updateEstimate(_sfdTemp[i], corrTrialSFD);
		}
	} else {
		updateEstimate((double)focus, incorrectTrialStringsRead);
		for (int i=0; i<=focus; i++){
			updateEstimate(_sfdTemp[i], incorrTrialSFD);
		}
	}
	for (int i=0; i<N_PAYOFFS; i++){
		p = Payoff::getPayoff(RT, acc, i);
		updateEstimate(p, payoffEstimate[i]);
	}

	// All SFD level updates (they are all here because we may care about response correctness for how we record SFDs)
	// update position SFD
	// The reason we keep track of _nRecordedSFDs is that if a trial ends mid-fixation, we don't want to record that fixation,
	// so looping upt to focus won't work
	for (int i=0; i<nFix; i++){
		// std::cout << "recording SFD"<<_sfdTemp[i] <<" at pos" << i << std::endl; 
		updateEstimate(_sfdTemp[i], posSFD[i]);
		// if (i == 0 || i == (STRINGS_PER_TRIAL-1)) continue; // dump SFDs from first and last positions -- they are often weird	
		// update global SFD estimate
		updateEstimate(_sfdTemp[i], globSFD);
		// update word and nonword SFD estimate
		if(drawnTrialWords[i]==-1){ // if nonword
			updateEstimate(_sfdTemp[i], nonwordSFD);
			// cannot update neighborhood density SFD estimate because we don't keep track of *which* nonword we drew. 
			// This is fixable but requires a few changes in a few different places so it can be TODO
		} else { // if word
			// update neighborhood density SFD:
			if (wordNeighDens[drawnTrialWords[i]]> neighSplit){
				updateEstimate(_sfdTemp[i], highNeighDensSFD);
			} else{
				updateEstimate(_sfdTemp[i], lowNeighDensSFD);
			}
			updateEstimate(_sfdTemp[i], wordSFD);
			// also update freq effects
			if (wordFreqs[drawnTrialWords[i]] > freqSplit){
				updateEstimate(_sfdTemp[i], highFreqSFD);
				if (acc==1) {
					updateEstimate(_sfdTemp[i], corrHighFreqSFD);
				} else{
					updateEstimate(_sfdTemp[i], incorrHighFreqSFD);
				}
			} 
			else {
				updateEstimate(_sfdTemp[i], lowFreqSFD); 
				if (acc==1) {
					updateEstimate(_sfdTemp[i], corrLowFreqSFD);
				} else{
					updateEstimate(_sfdTemp[i], incorrLowFreqSFD);
				}
			}
			// and spillover effect
			if (i>0){
				if (wordFreqs[drawnTrialWords[i-1]] > freqSplit){
					updateEstimate(_sfdTemp[i], highPrevFreqSFD);
				} 
				else {
					updateEstimate(_sfdTemp[i], lowPrevFreqSFD); 
				}

			}
		}
	}
}

void Recorder::updateEstimate(double newX, double estimate[3]){
	// remember, indices on estimate are MEAN, VAR, N
	double oldMean = estimate[0];
	double oldVar = estimate[1];
	estimate[0] = estimate[2]==0? newX : (oldMean + ((newX - oldMean) / (estimate[2] + 1)));
    estimate[1] = estimate[2]==0 ? 0 : ((1-(1/estimate[2]))*oldVar + (estimate[2]+1)*gsl_pow_2(estimate[0]-oldMean)); 
	estimate[2]++; 
}

// I guess we want a template here , eh?
void Recorder::updateNeigDensEst(double newX, std::vector<double> & estimate){
	double oldMean = estimate[0];
	double oldVar = estimate[1];
	estimate[0] = estimate[2]==0? newX : (oldMean + ((newX - oldMean) / (estimate[2] + 1)));
    estimate[1] = estimate[2]==0 ? 0 : ((1-(1/estimate[2]))*oldVar + (estimate[2]+1)*gsl_pow_2(estimate[0]-oldMean)); 
	estimate[2]++; 
}

#ifndef IGNORE_RCPP
using namespace Rcpp; 

void Recorder::setTrialInfoForR(NumericVector tw, int tt){
	for (int i =0; i<STRINGS_PER_TRIAL; i++){
		drawnTrialWords[i] = tw[i];
	}
	trialType = tt;
}

XPtr<Recorder> Recorder::returnPointerForR(){
	return XPtr<Recorder>(this, false); 
 }

NumericVector Recorder::poorMansWrap(double* toWrap){ // todo: learn how to Rcpp::wrap properly
	NumericVector o(3); 
	for (int i = 0; i < 3; i++){
		o[i] = toWrap[i];
	}
	return o; 
}

NumericVector Recorder::getglobRT_R(){
	return(poorMansWrap(globRT));
}

NumericVector Recorder::getglobAcc_R(){
	return(poorMansWrap(globAcc)); 
}

NumericVector Recorder::getglobSFD_R(){
	return(poorMansWrap(globSFD)); 
}

NumericMatrix Recorder::getposSFD_R(){
	NumericMatrix o(STRINGS_PER_TRIAL, 3);
	for (int i = 0; i < STRINGS_PER_TRIAL; i++){
		for (int j= 0; j<3; j++){
			o(i,j) = posSFD[i][j]; 
		}
	}
	return o;

}

NumericVector Recorder::getlowPrevFreqSFD_R(){
	return(poorMansWrap(lowPrevFreqSFD)); 
}

NumericVector Recorder::gethighPrevFreqSFD_R(){
	return(poorMansWrap(highPrevFreqSFD)); 
}


NumericVector Recorder::getlowFreqSFD_R(){
	return(poorMansWrap(lowFreqSFD)); 
}

NumericVector Recorder::getWordSFD_R(){
	return(poorMansWrap(wordSFD)); 
}

NumericVector Recorder::getnonwordSFD_R(){
	return(poorMansWrap(nonwordSFD)); 
}


NumericVector Recorder::gethighFreqSFD_R(){
	return(poorMansWrap(highFreqSFD)); 
}

NumericVector Recorder::getwordRT_R(){
	return(poorMansWrap(wordRT)); 
}

NumericVector Recorder::getnonwordRT_R(){
	return(poorMansWrap(nonwordRT)); 
}

NumericVector Recorder::getwordAcc_R(){
	return(poorMansWrap(wordAcc)); 
}

NumericVector Recorder::getnonwordAcc_R(){
	return(poorMansWrap(nonwordAcc)); 
}

NumericMatrix Recorder::getPayoff_R(){
	NumericMatrix o(N_PAYOFFS, 3);
	for (int i = 0; i<N_PAYOFFS; i++){
		for (int j = 0; j < 3; j++){
			o(i,j) = payoffEstimate[i][j];
		}
	}
	return o; 
}

void Recorder::generateFakeRecords(){
	for (int i=0; i <3; i++){
		globRT[i] = i;
		globAcc[i] = i;
		globSFD[i] = i; 
		lowFreqSFD[i] = i; 
		highFreqSFD[i] = i; 
		lowPrevFreqSFD[i] = i; 
		highPrevFreqSFD[i] = i; 
		wordRT[i] = i;
		nonwordRT[i] = i;
		wordSFD[i] = i;
		nonwordSFD[i] = i;
		wordAcc[i] = i;
		nonwordAcc[i] = i;
		for (int j = 0; j<N_PAYOFFS; j++){
			payoffEstimate[j][i] = i; 
		}
		for (int j = 0; j < STRINGS_PER_TRIAL; j++){
			posSFD[j][i] = i; 
		}
	}
}

Recorder::Recorder(SEXP wf, SEXP words, SEXP nonwords){
	NumericVector wFreqs(wf); 
	CharacterVector wdsVec(words);
	CharacterVector nwdsVec(nonwords);
	std::string wds[N_WORDS];
	std::string nwds[N_NONWORDS];
	for (int i = 0; i < N_WORDS; i++){
		wordFreqs[i] = wFreqs(i); 
		wds[i] = wdsVec[i];
	}
	for (unsigned i=0; i<N_NONWORDS; i++){
		nwds[i] = nwdsVec[i];
	}
	_computeLexNeighborhoodSize(wds, nwds, wordNeighDens, nonwordNeighDens);
	freqSplit = findFreqSplit(); 
	reset();
}

RCPP_MODULE(recorder_mod){
// eventually might want to expose everything for testing.  For now, expose just public stuff
  class_<Recorder>("Recorder")

  .constructor<SEXP,SEXP,SEXP>()

  .method("recordTrialEnd", &Recorder::recordTrialEnd)
  .method("recordFixationEnd", &Recorder::recordFixationEnd)
  .method("returnPointerForR", &Recorder::returnPointerForR)
  .method("generateFakeRecords", &Recorder::generateFakeRecords)
  .method("setTrialInfo", &Recorder::setTrialInfoForR)
  .method("reset", &Recorder::reset)
  .method("getPtr", &Recorder::returnPointerForR)
  .property("globRT", &Recorder::getglobRT_R)
  .property("globAcc", &Recorder::getglobAcc_R)
  .property("globSFD", &Recorder::getglobSFD_R)
  .property("posSFD", &Recorder::getposSFD_R)
  .property("lowFreqSFD", &Recorder::getlowFreqSFD_R)
  .property("highFreqSFD", &Recorder::gethighFreqSFD_R)
  .property("lowPrevFreqSFD", &Recorder::getlowPrevFreqSFD_R)
  .property("highPrevFreqSFD", &Recorder::gethighPrevFreqSFD_R)  
  .property("wordRT", &Recorder::getwordRT_R)
  .property("nonwordRT", &Recorder::getnonwordRT_R)
  .property("wordAcc", &Recorder::getwordAcc_R)
  .property("nonwordAcc", &Recorder::getnonwordAcc_R)
  .property("payoffEstimate", &Recorder::getPayoff_R)
  .property("wordSFD", &Recorder::getWordSFD_R)
  .property("nonwordSFD", &Recorder::getnonwordSFD_R)
  ;
}
#endif
