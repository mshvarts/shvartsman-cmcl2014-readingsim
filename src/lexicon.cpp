#include "lexicon.h"


#ifdef STATIC_CLUSTER
void loadWordsNoDisk(std::string words[N_WORDS], double freqs[N_WORDS], int nFileLines){
// this is a clever O(n) way to sample k out of n items  
  int stringsNeeded = N_WORDS;
  int stringsRemaining = nFileLines; 
  int i = 0, j = 0;
  double prob; 
  double r; 
  srand((unsigned)time(NULL));
  while(j<nFileLines && stringsNeeded > 0){
    prob = stringsNeeded / (double)stringsRemaining; 
    r = ((double)rand()/(double)RAND_MAX);
    if(r<=prob){
      words[i] = WORDS[j]; 
      freqs[i] = FREQS[j];
      stringsNeeded--;
      i++; 
    }
    stringsRemaining--;
    j++; 
  }
  if(i < (N_WORDS)){
    throw std::logic_error("don't have the right # of words!");
  }
}

void loadNonwordsNoDisk(std::string nonwords[N_NONWORDS], int nFileLines){
// this is a clever O(n) way to sample k out of n items  
  int stringsNeeded = N_NONWORDS;
  int stringsRemaining = nFileLines; 
  int i = 0, j=0;
  double prob; 
  double r; 
  while(j < nFileLines && stringsNeeded > 0){
    prob = stringsNeeded / (double)stringsRemaining; 
    r = ((double)rand()/(double)RAND_MAX);
    if(r<=prob){
      nonwords[i] = NONWORDS[j];
      stringsNeeded--;
      i++; 
    }
    stringsRemaining--;
    j++;
  }
  if(i < (N_NONWORDS)){
    throw std::logic_error("don't have the right # of nonwords!");
  }
}
#else
void loadWords(std::string words[N_WORDS], double freqs[N_WORDS], std::string wFileName, int nFileLines){
// this is a clever O(n) way to sample k out of n items  
  int stringsNeeded = N_WORDS;
  int stringsRemaining = nFileLines; 
  int i = 0;
  double prob; 
  double r; 
  std::string line, word; 
  std::ifstream infile;
  infile.open(wFileName.c_str(), std::ifstream::in);
  srand((unsigned)time(NULL));
  while(infile.good() && stringsNeeded > 0){
    prob = stringsNeeded / (double)stringsRemaining; 
    getline(infile, line);
    r = ((double)rand()/(double)RAND_MAX);
    if(r<=prob){
      words[i] = line.substr(0, CHARS_PER_STRING);
      freqs[i] = atof(line.substr(CHARS_PER_STRING).c_str());
      stringsNeeded--;
      i++; 
    }
    stringsRemaining--;
  }
  if(i < (N_WORDS)){
    throw std::logic_error("don't have the right # of words!");
  }
  infile.close();
}

void loadNonwords(std::string nonwords[N_NONWORDS], std::string nwFileName, int nFileLines){
// this is a clever O(n) way to sample k out of n items  
  int stringsNeeded = N_NONWORDS;
  int stringsRemaining = nFileLines; 
  int i = 0;
  double prob; 
  double r; 
  std::string line, word; 
  std::ifstream infile;
  infile.open(nwFileName.c_str(), std::ifstream::in);
  while(infile.good() && stringsNeeded > 0){
    prob = stringsNeeded / (double)stringsRemaining; 
    getline(infile, line);
    r = ((double)rand()/(double)RAND_MAX);
    if(r<=prob){
      nonwords[i] = line;
      stringsNeeded--;
      i++; 
    }
    stringsRemaining--;
  }
  if(i < (N_NONWORDS)){
    throw std::logic_error("don't have the right # of nonwords!");
  }
}
#endif
#ifndef IGNORE_RCPP
using namespace Rcpp; 

// [[Rcpp::export]]
List loadWordsR(SEXP wFileName, SEXP wFileLines){
  std::string words[N_WORDS];
  std::string filename = as<std::string>(wFileName);
  int nlines = as<int>(wFileLines);
  double freqs[N_WORDS];
  loadWords(words, freqs, filename, nlines);
  CharacterVector w(N_WORDS);
  NumericVector f(N_WORDS);
  for (int i=0; i<N_WORDS; i++){
    w[i] = words[i];
    f[i] = freqs[i];
  } 
  return(Rcpp::List::create(Rcpp::Named( "words" )=w, Rcpp::Named("freqs")=f));
}

// [[Rcpp::export]]
CharacterVector loadNonwordsR(SEXP nwFileName, SEXP nwFileLines){
  std::string filename = as<std::string>(nwFileName);
  int nlines = as<int>(nwFileLines);
  std::string nonwords[N_NONWORDS];
  loadNonwords(nonwords, filename, nlines);
  CharacterVector nw(N_NONWORDS);
  for (int i=0; i<N_NONWORDS; i++){
    nw[i] = nonwords[i];
  } 
  return(nw);
}

#endif
