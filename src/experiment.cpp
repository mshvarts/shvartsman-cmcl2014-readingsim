#include "experiment.h"

/*
double Experiment::getPayoffMean(){
    return payoffMean;
}

double Experiment::getPayoffSD(){
    return sqrt(payoffVariance);
}
*/
Experiment::Experiment(double ebl, double spt, double set, double motorTime, double coefVar, bool perceptPrim, double perceptNoise, double memNoise, double updateNoise, std::string *words, std::string *nonwords, double *wordFreqs, double trialWordPrior, double probWordTrial, double maxTrialDuration, double timePerStep, int conditioningPost): words(words), nonwords(nonwords), probWordTrial(probWordTrial), maxTrialDuration(maxTrialDuration){
    arch = new Architecture(ebl, spt, set, motorTime, coefVar, perceptPrim, timePerStep);
    belief = new Belief(words, nonwords, wordFreqs, trialWordPrior, conditioningPost, updateNoise);
    sampler = new Sampler(perceptNoise, memNoise);
    pol = NULL;
    ownsRng = false;
    for (int i=0; i< STRINGS_PER_TRIAL; i++){
        trialStrings[i] = "    ";
    }
  if(!RNG::rngIsActive()){
    RNG::setupRng();
    ownsRng = true;
  }
}

void Experiment::setPerceptNoise(double perceptNoise){
    sampler->setPerceptNoise(perceptNoise);
}

void Experiment::setMemNoise(double memNoise){
    sampler->setMemNoise(memNoise);
}

void Experiment::setUpdateNoise(double updateNoise){
    belief->setUpdateNoise(updateNoise);
}

void Experiment::setSPT(double spt){
    arch->setSPT(spt); 
}
void Experiment::setPerPrim(bool perPrim){
    arch->setPerPrim(perPrim);
}

Experiment::~Experiment(){
    delete arch;
    delete belief;
    delete sampler;
    delete pol;
  if (ownsRng){
    RNG::freeRng();
  }
}

void Experiment::setPolicy(double sacA, double sacB, double memA, double memB, double taskYes, double taskNo, int sampDeadline){
    delete [] pol;
    pol = new Policy(sacA, sacB, memA, memB, taskYes, taskNo, sampDeadline);
    // automatically zero out payoff to protect us from our own stupidity :)
    // payoffMean = 0;
    // payoffVariance = 0;
}

void Experiment::runTrials(int nTrials, Recorder * rec){
    Trial trial(arch, pol, sampler, belief, rec, true); // storeDroppedSamples=true
    int trialType = 0;
    for (int i=0; i<nTrials; i++){
        trialType = RNG::rbernoulli(probWordTrial);
        pickTrialStrings(trialType, rec);  // pass recorder in because it needs to know string freqs for keeping track of freq effect
        trial.run(trialStrings, trialType);
    }
}

/*
//TODO: make an objective func version of the runTrials method we can use with fancy optim,
// plus do the payoff setting (maybe we should do it here?)
void Experiment::setPayoffScheme(double pspeed, double pacc, double cutoff){
    delete [] payoff;
    payoff = new Payoff(pspeed, pacc, cutoff);
    // automatically zero out payoff to protect us from our own stupidity :)
    payoffMean = 0;
    payoffVariance = 0;
}

void Experiment::resetPayoffs(){
    payoffMean = 0;
    payoffVariance = 0;
}

void Experiment::runTrials(int nTrials, Recorder * rec){
    Trial trial(arch, pol, sampler, belief, payoff, maxTrialDuration);
    int trialType = 0;
    for (int i=0; i<nTrials; i++){
        trialType = RNG::rbernoulli(probWordTrial);
        pickTrialStrings(trialType, rec);
        trial.run(trialStrings, trialType, rec);
    }
}


double objFunction(int nTrials,double sacA, double sacB, double memA, double memB, double taskYes, double taskNo, double pspeed, double pacc, double cutoff){
    if (payoff!=NULL){
        delete [] payoff;
    }
    if (pol!=NULL){
        delete [] pol;
    }
    payoff = new Payoff(pspeed, pacc, cutoff);
    pol = new Policy(sacA, sacB, memA, memB, taskYes, taskNo);
    Trial trial(arch, pol, sampler, belief, payoff, maxTrialDuration);
    int trialType = 0;
    double p, oldMean = 0;
    for (int i=1; i<=nTrials; i++){ // intentionally 1-indexed here, see http://mathworld.wolfram.com/SampleVarianceComputation.html
        trialType = RNG::rbernoulli(probWordTrial);
        pickTrialStrings(trialType);
        p = trial.run(trialStrings, trialType);
        oldMean = payoffMean;
        payoffMean = i==1? p : (p + ((p - oldMean) / (i + oldMean)));
    }
    return payoffMean;
}
*/

void Experiment::pickTrialStrings(int trialType, Recorder * rec){
    int nonwordPos = STRINGS_PER_TRIAL+1; // by default we never index by this (overriden below if we do draw a nonword)
    int drawnWordIds[STRINGS_PER_TRIAL];
    int numWordsDrawn=0;
    int nonwordID;
    if (trialType == 0){
        nonwordPos = RNG::runif(STRINGS_PER_TRIAL);
    }
    for (int i = 0; i < STRINGS_PER_TRIAL; i++){
        if (i!= nonwordPos){ // a word!
            // draw a string
            drawnWordIds[i] = RNG::runif(N_WORDS);
            // enforcing no repeated words is a pain in the butt:
            // loop through all the words drawn so far
            for (int k = 0; k < numWordsDrawn; k++){
                // If our latest word drawn is equal to any of the previous
                if(drawnWordIds[numWordsDrawn] == drawnWordIds[k]){
                    // draw a new one
                    drawnWordIds[numWordsDrawn] = RNG::runif(N_WORDS);
                    // ... and check the whole list again
                    k = 0;
                // loop should only be escaped if current is not equal to any of previous;
                }
            }
            // now that we know the new word is safe, copy it in
            for (int j = 0; j < CHARS_PER_STRING; j++){
                trialStrings[i][j] = words[drawnWordIds[numWordsDrawn]][j];
            }

        } else { // nonword here
            nonwordID = RNG::runif(N_NONWORDS);
            for (int j = 0; j < CHARS_PER_STRING; j++){
                trialStrings[i][j] = nonwords[nonwordID][j];
            }
            drawnWordIds[i] = -1; // this tells the recorder there's a nonword here
        }
        numWordsDrawn++;
    }
    // tell the recorder which words we drew (so it can record SFD by freq, word/nonword etc)
    rec->setTrialInfo(drawnWordIds, trialType);
}

#ifndef IGNORE_RCPP
using namespace Rcpp;

void Experiment::pickTrialStringsR(int trialType, SEXP rec){
    XPtr< Recorder > recorder(rec);
    pickTrialStrings(trialType, recorder);
}

Experiment::Experiment(SEXP params, SEXP words, SEXP nonwords, SEXP freqs){
    List par(params);
    double ebl = as<double>(par["ebl"]);
    double spt = as<double>(par["spt"]);
    double set = as<double>(par["set"]);
    double motorTime = as<double>(par["motorTime"]);
    double coefVar = as<double>(par["coefVar"]);
    double perceptPrim = as<double>(par["perceptPrim"]);
    double perceptNoise = as<double>(par["perceptNoise"]);
    double memNoise = as<double>(par["memNoise"]);
    double trialWordPrior = as<double>(par["trialWordPrior"]);
    double timePerStep = as<double>(par["timePerStep"]);
    probWordTrial = as<double>(par["probWordTrial"]);
    maxTrialDuration = as<double>(par["maxTrialDuration"]);
    int conditioningPost = as<double>(par["conditioningPost"]);
    arch = new Architecture(ebl, spt, set, motorTime, coefVar, perceptPrim, timePerStep);
    // very ugly call here to forward parameters to the R-oriented constructor for belief. I'm vaguely okay with it
    // because it's for testing only.
    belief = new Belief(words, nonwords, freqs, wrap(List::create(Named("trialWordPrior")=trialWordPrior, Named("conditioningPost")=conditioningPost)));
    sampler = new Sampler(perceptNoise, memNoise);
    pol = NULL;
    ownsRng = false;
    if(!RNG::rngIsActive()){
        RNG::setupRng();
        ownsRng = true;
    }
    payoffMean = 0;
    payoffVariance = 0;
}
void Experiment::runTrialsForR(int nTrials, SEXP recR){
    XPtr< Recorder > recPtr(recR);
    Recorder * rec = recPtr;
    Trial trial(arch, pol, sampler, belief, rec, maxTrialDuration);
    int trialType = 0;
    for (int i=0; i<nTrials; i++){
        trialType = RNG::rbernoulli(probWordTrial);
        pickTrialStrings(trialType, rec);  // pass recorder in because it needs to know string freqs for keeping track of freq effect
        trial.run(trialStrings, trialType);
    }
}

// int Experiment::runTrialsWithRInterrupt(int nTrials, Recorder * rec){
//     Trial trial(arch, pol, sampler, belief, rec, maxTrialDuration);
//     int trialType = 0;
//     for (int i=0; i<nTrials; i++){
//         if (i % 10 == 0 && checkInterrupt()) return 1;
//         trialType = RNG::rbernoulli(probWordTrial);
//         pickTrialStrings(trialType, rec);  // pass recorder in because it needs to know string freqs for keeping track of freq effect
//         trial.run(trialStrings, trialType);
//     }
//     return 0;
// }

// // https://stat.ethz.ch/pipermail/r-devel/2011-April/060702.html
// static void chkIntFn(void *dummy) {
//   R_CheckUserInterrupt();
// }
// // this will call the above in a top-level context so it won't longjmp-out of your context
// bool checkInterrupt() {
//   return (R_ToplevelExec(chkIntFn, NULL) == FALSE);
// }

RCPP_MODULE(experiment_mod){
// eventually might want to expose everything for testing.  For now, expose just public stuff

  class_<Experiment>("Experiment")

  .constructor<SEXP,SEXP,SEXP,SEXP>("Sets all experiment params")

  .method("runTrials", &Experiment::runTrialsForR)
  //.method("getPayoffMean", &Experiment::getPayoffMean)
  //.method("getPayoffSD", &Experiment::getPayoffSD)
  .method("pickTrialStrings",&Experiment::pickTrialStringsR)
  .method("setPolicy", &Experiment::setPolicy)
  //.method("setPayoffScheme", &Experiment::setPayoffScheme)
  //.method("resetPayoffs", &Experiment::resetPayoffs)
//  .method("pickTrialStrings", &Experiment::pickTrialStrings)
  // need here: something that exposes the trial strings?
  ;
}
#endif
