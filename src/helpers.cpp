#include "helpers.h"
#include <iostream>

double sdOfGaussSum(double sd1, double sd2){
	return sqrt(gsl_pow_2(sd1) + gsl_pow_2(sd2)); 
} 

void _computeLexNeighborhoodSize(std::string words[N_WORDS], std::string nonwords[N_NONWORDS], int wNeighSize[N_WORDS], int nwNeighSize[N_NONWORDS]){
  std::vector<std::string> working(N_WORDS+N_NONWORDS); 
  for (int i=0; i<N_WORDS; i++){
    wNeighSize[i] = 0 ;
  }
  for (int i=0; i<N_NONWORDS; i++){
    nwNeighSize[i] = 0;
  }

  for (int letter=0; letter<CHARS_PER_STRING; letter++){
    // create a 'working' lexicon with one character missing 
    for (int i=0; i < N_WORDS; i++){
      working[i] = words[i];
      working[i].erase(letter, 1);
    }
    for (int i=N_WORDS; i < N_WORDS+N_NONWORDS; i++){
      working[i] = nonwords[i-N_WORDS];
      working[i].erase(letter, 1);
    }
    // for each word, see how many are equal to it with this char missing
    for (int i=0; i < N_WORDS; i++){
      // first check the words
      for (int j=0; j < N_WORDS; j++){
        if (i==j) continue;
        // std::cout << working[i] << " " << working[j] << std::endl;
        if(working[i]==working[j]) wNeighSize[i]++;
      }
      // then nonwords
      for (int j=N_WORDS; j < N_WORDS+N_NONWORDS; j++){
        if (i==j) continue;
        // std::cout << working[i] << " " << working[j] << std::endl;
        if(working[i] == working[j]) wNeighSize[i]++; 
      }
    }
    // then do the same thing for each nonword
    for (int i=N_WORDS; i < N_WORDS+N_NONWORDS; i++){
      // first check the words
      for (int j=0; j < N_WORDS; j++){
        if (i==j) continue;
        // std::cout << working[i] << " " << working[j] << std::endl;
        if(working[i]==working[j]) nwNeighSize[i-N_WORDS]++;
      }
      // then nonwords
      for (int j=N_WORDS; j < N_WORDS+N_NONWORDS; j++){
        if (i==j) continue;
        // std::cout << working[i] << " " << working[j] << std::endl;
        if(working[i] == working[j]) nwNeighSize[i-N_WORDS]++; 
      }
    }
  }
}
