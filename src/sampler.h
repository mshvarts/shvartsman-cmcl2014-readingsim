// include guard
#ifndef SAMPLER_H
#define SAMPLER_H

// no forward declarations needed

// includes we need
#include "rng.h" // this is wrapping the RNG
#include "constants.h" // for all the sizes
#include <string>
#ifndef IGNORE_RCPP
    #include <Rcpp.h>
#endif

class Sampler {
    public:
        static double samp[CHARS_PER_STRING][ALPHABET_SIZE];
        void drawPerceptSample();
        void drawMemSample();
        void setPerceptNoise(double pn);
        void setMemNoise(double mn);
        double getPerceptNoise();
        double getMemNoise();
        void reset(std::string newString);
        Sampler(double perceptNoise, double memNoise);
        ~Sampler();
        #ifndef IGNORE_RCPP
            Rcpp::NumericMatrix sampGetterForR();
            Rcpp::NumericMatrix trueStringGetterForR();
            Rcpp::List memoryGetterForR();
            Sampler(SEXP params);
            Rcpp::XPtr<Sampler> returnPointerForR();
        #endif
    private:
        static double trueString[CHARS_PER_STRING][ALPHABET_SIZE];
        static double mem[MEMORY_SIZE][CHARS_PER_STRING][ALPHABET_SIZE];
        std::string stringRepr;
        double perceptNoise;
        double memNoise;
        bool ownsRng;
        int memIndex;
        void stringToBitvector();

};

#endif
