#include "policy.h"
#include <iostream>

// double Policy::_H(double posterior)
// {
// 	if(posterior == 0) return 0; // just in case, though this should only happen due to floating point error
// 	if(posterior == 1) return 0; // just in case, though this should only happen due to floating point error
//     return -(posterior * (gsl_sf_log(posterior)/gsl_sf_log(2)) + ((1-posterior)* (gsl_sf_log(1-posterior)/gsl_sf_log(2))));
// }

bool Policy::moveEyes(double wordBelief, SamplingTypes sType, int nSamps){
  if (nSamps == _sampDeadline) return true; 
    double threshold = 0;
    switch (sType) {
        case Perception:
            threshold = saccadeA;
            break;
        case Memory:
            threshold = memoryA;
            break;
        case FreePerception:
            return false;
    }
    return wordBelief > threshold;
}

bool Policy::respond(double trialBelief){
    if (trialBelief > 0.5 ) return trialBelief > taskYes;
    else return trialBelief < (1-taskNo);
}

// double Policy::getSaccadeThresh(Belief* b){
//     // return fmin(maxThresh, saccadeA + saccadeB*H(posterior));
//     return saccadeA;
// }

// double Policy::getMemoryThresh(Belief * b){
//     // return fmin(maxThresh, memoryA + memoryB * H(posterior));
//     return memoryA;
// }

// double Policy::getTaskThresh(Belief * b){
//     double tb = b->getTrialBelief();
//     double thresh = tb > 0.5 ? taskYes : taskNo;
//     return thresh;
// }

bool Policy::isMemoryModel(){
    return (memoryA!=0);
}

Policy::Policy(double saccadeA, double saccadeB, double memoryA, double memoryB, double taskYes, double taskNo, int sampDeadline): saccadeA(saccadeA), saccadeB(saccadeB), memoryA(memoryA), memoryB(memoryB), taskYes(taskYes), taskNo(taskNo), _sampDeadline(sampDeadline) {
	maxThresh = MAX_THRESH;
}


#ifndef IGNORE_RCPP
using namespace Rcpp;

Policy::Policy(SEXP params){
	List par(params);
	try {
		saccadeA = as<double>(par["saccadeA"]);
		saccadeB = as<double>(par["saccadeB"]);
		memoryA = as<double>(par["memoryA"]);
		memoryB = as<double>(par["memoryB"]);
		taskYes = as<double>(par["taskYes"]);
		taskNo = as<double>(par["taskNo"]);
		maxThresh = as<double>(par["maxThresh"]);
    _sampDeadline = as<double>(par["sampDeadline"]);
  	}
  	catch (const index_out_of_bounds& e){
    forward_exception_to_r(std::runtime_error("Missing architecture parameters!"));
  	}
}

XPtr<Policy> Policy::returnPointerForR(){
  return XPtr<Policy>(this, false);
}


RCPP_MODULE(policy_mod){
// eventually might want to expose everything for testing.  For now, expose just public stuff
  class_<Policy>("Policy")

  .constructor<SEXP>("Sets policy params")
	// .method("getSaccadeThresh", &Policy::getSaccadeThresh)
	// .method("getMemThresh", &Policy::getMemoryThresh)
	// .method("isMemoryModel", &Policy::isMemoryModel)
	.method("getPtr", &Policy::returnPointerForR)
  ;
}
#endif
