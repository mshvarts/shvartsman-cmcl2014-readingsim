#include "architecture.h"

double Architecture::drawEBL()
{
    int unrounded = static_cast<int>(RNG::rGamma(eyeBrainLagMean, coefVariation*eyeBrainLagMean));
    int remain = unrounded % TIMEPERSTEP; 
    int rounded = remain > (TIMEPERSTEP/2) ? unrounded-remain + TIMEPERSTEP : unrounded - remain;
    return rounded; 
    // return RNG::rGamma(eyeBrainLagMean, coefVariation*eyeBrainLagMean);
}

double Architecture::drawSET()
{
    // return RNG::rGamma(saccadeExecTimeMean, coefVariation*saccadeExecTimeMean);
  int unrounded = static_cast<int>(RNG::rGamma(saccadeExecTimeMean, coefVariation*saccadeExecTimeMean));
  int remain = unrounded % TIMEPERSTEP; 
  int rounded = remain > (TIMEPERSTEP/2) ? unrounded-remain + TIMEPERSTEP : unrounded - remain;
  return rounded; 
}

double Architecture::drawSPT()
{
    // return RNG::rGamma(saccadeProgTimeMean, coefVariation*saccadeProgTimeMean);
  int unrounded =static_cast<int>(RNG::rGamma(saccadeProgTimeMean, coefVariation*saccadeProgTimeMean));
  int remain = unrounded % TIMEPERSTEP; 
  int rounded = remain > (TIMEPERSTEP/2) ? unrounded-remain + TIMEPERSTEP : unrounded - remain;
  return rounded; 
}

double Architecture::drawMotorTime()
{
    // return RNG::rGamma(motorTimeMean, coefVariation*motorTimeMean);
  int unrounded = static_cast<int>(RNG::rGamma(motorTimeMean, coefVariation*motorTimeMean));
  int remain = unrounded % TIMEPERSTEP; 
  int rounded = remain > (TIMEPERSTEP/2) ? unrounded-remain + TIMEPERSTEP : unrounded - remain;
  return rounded; 
}

bool Architecture::withPerceptualPrimacy(){
    return perceptualPrimacy;
}

double Architecture::getTimePerStep(){
  return timePerStep; 
}

void Architecture::setSPT(double spt){
  saccadeProgTimeMean = spt; 
}
void Architecture::setPerPrim(bool perPrim){
  perceptualPrimacy = perPrim;
}

Architecture::Architecture(double eyeBrainLagMean, double saccadeProgTimeMean, double saccadeExecTimeMean, double motorTimeMean, double coefVariation, bool perceptualPrimacy, double timePerStep): eyeBrainLagMean(eyeBrainLagMean), saccadeExecTimeMean(saccadeExecTimeMean), saccadeProgTimeMean(saccadeProgTimeMean), motorTimeMean(motorTimeMean), coefVariation(coefVariation), perceptualPrimacy(perceptualPrimacy), timePerStep(timePerStep) {
  ownsRng = false;   
  if(!RNG::rngIsActive()){
    RNG::setupRng();
    ownsRng = true; 
  }
}

Architecture::~Architecture(){
  if (ownsRng){
    RNG::freeRng();
  }
}

#ifndef IGNORE_RCPP
using namespace Rcpp; 
Architecture::Architecture(SEXP params){
  ownsRng = false; 
  List par(params); 
  try{
  eyeBrainLagMean = as<double>(par["eyeBrainLagMean"]);
  saccadeExecTimeMean = as<double>(par["saccadeExecTimeMean"]);  
  saccadeProgTimeMean = as<double>(par["saccadeProgTimeMean"]);
  motorTimeMean = as<double>(par["motorTimeMean"]);
  coefVariation = as<double>(par["coefVariation"]);
  perceptualPrimacy = as<bool>(par["perceptualPrimacy"]);
  timePerStep = as<double>(par["timePerStep"]);
  }
  catch (const index_out_of_bounds& e){
    forward_exception_to_r(std::runtime_error("Missing architecture parameters!"));
  }

  if(!RNG::rngIsActive()){
    RNG::setupRng();
    ownsRng = true; 
  }
}


XPtr<Architecture> Architecture::returnPointerForR(){
  return XPtr<Architecture>(this, false); 
}

double archPtrTest(SEXP archS){
  XPtr< Architecture > archPtr(archS);
  return archPtr->getTimePerStep();
}

RCPP_MODULE(arch_mod){
// eventually might want to expose everything for testing.  For now, expose just public stuff

  class_<Architecture>("Architecture")

  .constructor<SEXP>("Sets all architectural params")

  .method("drawEBL", &Architecture::drawEBL)
  .method("drawSET", &Architecture::drawSET)
  .method("drawSPT", &Architecture::drawSPT)
  .method("drawMotorTime", &Architecture::drawMotorTime)
  .method("withPerceptualPrimacy", &Architecture::withPerceptualPrimacy)
  .method("getTimePerStep", &Architecture::getTimePerStep)
  .method("getPtr", &Architecture::returnPointerForR)
  ;
  function("archPtrTest", &archPtrTest);

}

 #endif
