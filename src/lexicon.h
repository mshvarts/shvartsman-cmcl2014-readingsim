#ifndef LEXICON_H
#define LEXICON_H

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <time.h> // for time(NULL) on windows

#include "../src/constants.h"

#ifndef IGNORE_RCPP
    #include <Rcpp.h>
#endif
#ifdef STATIC_CLUSTER
    #include "../static/allstrings.h"
#endif

void loadWordsNoDisk(std::string words[N_WORDS], double freqs[N_WORDS], int nFileLines);
void loadNonwordsNoDisk(std::string nonwords[N_NONWORDS], int nFileLines);

void loadWords(std::string words[N_WORDS], double freqs[N_WORDS], std::string wFileName, int nFileLines);

void loadNonwords(std::string nonwords[N_NONWORDS], std::string nwFileName, int nFileLines);




#endif
