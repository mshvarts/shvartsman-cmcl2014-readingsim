#ifndef CONFIG
#define CONFIG

#include <string>

// DEPRECATED; ALL-NON CONSTANT CONFIG IS IN MAIN.CPP FOR A PARTICULAR SET OF SIMS

int WORDFILE_LINES = 1500;
int NONWORDFILE_LINES = 10424; 
std::string WORDFILE_NAME = "kf-words.txt";
std::string NONWORDFILE_NAME = "arc-nonwords.txt";
int NTRIALS = 1000; 
double EBL = 50;
double SPT = 100;
double SET = 40;
double MOTOR_TIME = 250;
double COEFVAR = 0.3;
double PERCEPTPRIM = true;
double PNOISE = 1.2;
double MNOISE = 1.2;
double TRIALWORDPRIOR = 0.5;
double PROBWORDTRIAL = 0.5;
double MAXTRIALDUR = 20000;
double TIMEPERSTEP = 10;
double CONDITIOININGPOST = 1;
int N_PAYOFFS = 3; 
double PSPEEDS[N_PAYOFFS] = {4.5, 5.6, 8};
double PACCS[N_PAYOFFS] = {25, 50, 125};
double CUTOFFS[N_PAYOFFS] = {5000, 5000, 5000};
std::string PLABELS[N_PAYOFFS] = {"SPEED", "BAL", "ACC"}; 

#endif