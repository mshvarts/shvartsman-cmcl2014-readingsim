#ifndef RECORDER_H
#define RECORDER_H

#include "payoff.h"
#include <gsl/gsl_math.h> // for pow, needed for the SD calc
#include "constants.h"
#include <algorithm> // for nth_element() for median, for max_element for neighDens
#include <vector> 

#ifndef IGNORE_RCPP
#include <Rcpp.h>
#include "helpers.h" // when we start this up through R we compute neighborhood densities here
#endif

using std::vector;

class Recorder{
	public:
		//Recorder(Payoff payoff, double freqs[N_WORDS]); // version for objective func implementation, TBA
		//double getPayoff(); // version for objective func implementation, TBA
		Recorder(double *wf, int *wnd, int *nwnd);
		void setTrialInfo(int tw[STRINGS_PER_TRIAL], int tt);
		void recordFixationEnd(double sfd, int pos);
		void recordTrialEnd(double RT, int response, int focus, int nFix);
		void savePerSamps(int count);
		void saveMemSamps(int count);
		void saveDroppedSamps(int count);
		void saveSampsToDecision(int count, int focus);
		void reset(); 
		// all [3] arrays for mean, var, n
		// ideally all of these should be private with accessor functions (TODO)
		double globRT[3];
		double globAcc[3];
		double globSFD[3]; 
		double posSFD[STRINGS_PER_TRIAL][3];
		double lowFreqSFD[3]; 
		double highFreqSFD[3]; 
		
		double corrTrialSFD[3];
		double incorrTrialSFD[3];

		double corrLowFreqSFD[3]; 
		double corrHighFreqSFD[3]; 
		double incorrLowFreqSFD[3]; 
		double incorrHighFreqSFD[3]; 

		double perSampCount[3]; 
		double memSampCount[3]; 
		double droppedSampCount[3]; 
		double sampsToDecision[3]; 
		double sampsToDecisionLowFreq[3];
		double sampsToDecisionHighFreq[3];

		double lowPrevFreqSFD[3]; 
		double highPrevFreqSFD[3]; 

		double wordRT[3];
		double nonwordRT[3];
		double wordSFD[3];
		double nonwordSFD[3];
		
		double wordAcc[3];
		double nonwordAcc[3];
		
		double payoffEstimate[N_PAYOFFS][3]; 

		std::vector<std::vector<double > > neighDensSFD; 

		double stringsRead[3];
		double wordTrialStringsRead[3];
		double nonwordTrialStringsRead[3];
		double correctTrialStringsRead[3];
		double incorrectTrialStringsRead[3];

		double lowNeighDensSFD[3];
		double highNeighDensSFD[3];

		#ifndef IGNORE_RCPP
		Recorder(SEXP wf, SEXP words, SEXP nonwords); 
		Rcpp::XPtr< Recorder > returnPointerForR(); 
		Rcpp::NumericVector getglobRT_R();
		Rcpp::NumericVector getglobAcc_R();
		Rcpp::NumericVector getglobSFD_R();
		Rcpp::NumericMatrix getposSFD_R();
		Rcpp::NumericVector getWordSFD_R();
		Rcpp::NumericVector getnonwordSFD_R();
		Rcpp::NumericVector getlowFreqSFD_R();
		Rcpp::NumericVector gethighFreqSFD_R();
		Rcpp::NumericVector getwordRT_R();
		Rcpp::NumericVector getnonwordRT_R();
		Rcpp::NumericVector getwordAcc_R();
		Rcpp::NumericVector getnonwordAcc_R();
		Rcpp::NumericMatrix getPayoff_R();
		Rcpp::NumericVector getlowPrevFreqSFD_R();
		Rcpp::NumericVector gethighPrevFreqSFD_R();
		Rcpp::NumericVector poorMansWrap(double* toWrap);
		void generateFakeRecords(); 
		void setTrialInfoForR(Rcpp::NumericVector tw, int tt);
		#endif

	private: 
		void updateEstimate(double newX, double estimate[3]);
		// should be templated, argh
		void updateNeigDensEst(double newX, std::vector<double> &estimate);
		double findFreqSplit();
		double findNeighSplit();
		double trialType;
		double _sfdTemp[STRINGS_PER_TRIAL];
		double wordFreqs[N_WORDS]; // need to know this to compute word / nonword SFD and highfreq/lowfreq SFD
		int drawnTrialWords[STRINGS_PER_TRIAL];
		double freqSplit, neighSplit;
		double _findMedian(vector<double> freqs);
		int wordNeighDens[N_WORDS];
		int nonwordNeighDens[N_NONWORDS];

};

#endif
