#include "payoff.h" 


// Payoff::Payoff(double pSpeed, double pAcc, double cutoff): pSpeed(pSpeed), pAcc(pAcc), cutoff(cutoff) {}

double Payoff::getPayoff(double rt, double acc, int payoffID){
	double payoff; 
    if (rt > CUTOFFS[payoffID]){ // no penalty for responding too late, but time bonus 0
        payoff = (1 - acc) * PACCS[payoffID]; 
    }
    else {
        payoff = (CUTOFFS[payoffID]-rt) / 1000 * PSPEEDS[payoffID] - (1-acc)*PACCS[payoffID]; 
    }	
	return payoff; 
}

#ifndef IGNORE_RCPP
using namespace Rcpp; 
// Payoff::Payoff(SEXP params){
//   List par(params); 
//   pSpeed = as<double>(par["pSpeed"]);
//   pAcc = as<double>(par["pAcc"]);
//   cutoff = as<double>(par["cutoff"]);
// }

// XPtr<Payoff> Payoff::returnPointerForR(){
//   return XPtr<Payoff>(this); 
// }

RCPP_MODULE(payoff_mod){
// eventually might want to expose everything for testing.  For now, expose just public stuff

//   class_<Payoff>("Payoff")

//   .constructor<SEXP>("Sets payoff params")

// .method("getPayoff", &Payoff::getPayoff)
// .method("getPtr", &Payoff::returnPointerForR)
//   ;
  function("getPayoff", &Payoff::getPayoff);
}
#endif
