// include guard
#ifndef EXPERIMENT_H
#define EXPERIMENT_H

// forward declarations as needed
class Trial;
class Architecture;
class Strategy;
class Belief;
class Sampler;
class Recorder;

// includes we need
#ifndef IGNORE_RCPP
    #include <Rcpp.h>
#endif

#include <iostream>
#include <math.h> // for sqrt
#include <string>
#include "rng.h"
#include "architecture.h"
#include "belief.h"
#include "sampler.h"
#include "policy.h"
#include "payoff.h"
#include "recorder.h"
#include "trial.h"

// actual class
class Experiment{
    public:
        Experiment(double ebl, double spt, double set, double motorTime, double coefVar, bool perceptPrim, double perceptNoise, double memNoise, double updateNoise, std::string *words, std::string *nonwords, double *wordFreqs, double trialWordPrior, double probWordTrial, double maxTrialDuration, double timePerStep, int conditioningPost);
        ~Experiment();
        void runTrials(int nTrials, Recorder *recorder);
        //double objFunction(int nTrials,double sacA, double sacB, double memA, double memB, double taskYes, double taskNo, double pspeed, double pacc, double cutoff);
        double getPayoffMean();
        double getPayoffSD();
        void setPerceptNoise(double perceptNoise);
        void setMemNoise(double memNoise);
        void setUpdateNoise(double updateNoise);
        void setSPT(double spt);
        void setPerPrim(bool perPrim); 
        void setPolicy(double sacA, double sacB, double memA, double memB, double taskYes, double taskNo, int sampDeadline);
        //void setPayoffScheme(double pspeed, double pacc, double cutoff);
        //void resetPayoffs();
        #ifndef IGNORE_RCPP
            Experiment(SEXP params, SEXP words, SEXP nonwords, SEXP freqs);
            void runTrialsForR(int nTrials, SEXP recR);
            void pickTrialStringsR(int trialType, SEXP rec);
        #endif

    private:
        void pickTrialStrings(int trialType, Recorder * rec);
        Architecture *arch;
        Policy *pol;
        Belief *belief;
        Sampler *sampler;
        Recorder * recorder;
        std::string *words;
        std::string *nonwords;
        std::string trialStrings[STRINGS_PER_TRIAL];
        bool ownsRng;
        double payoffMean;
        double payoffVariance;
        double probWordTrial;
        double maxTrialDuration;
};

#endif
