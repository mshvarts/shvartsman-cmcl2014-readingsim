#ifndef HELPERS_H
#define HELPERS_H

#include <math.h> // for sqt
#include <gsl/gsl_math.h>
#include <vector>
#include <string>
#include "constants.h"

double sdOfGaussSum(double sd1, double sd2); 

void _computeLexNeighborhoodSize(std::string words[N_WORDS], std::string nonwords[N_NONWORDS], int wNeighSize[N_WORDS], int nwNeighSize[N_NONWORDS]);

#endif
