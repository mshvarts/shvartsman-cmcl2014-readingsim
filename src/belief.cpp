#include "belief.h"
#include <iostream>

double Belief::wordBelief[STRINGS_PER_TRIAL][N_WORDS] = {{0}};
double Belief::nonwordBelief[STRINGS_PER_TRIAL][N_NONWORDS] = {{0}};
double Belief::trialBelief[STRINGS_PER_TRIAL+1] = {0};
double Belief::wordCharDensity[N_WORDS][CHARS_PER_STRING][ALPHABET_SIZE] = {{{0}}};
double Belief::nonwordCharDensity[N_NONWORDS][CHARS_PER_STRING][ALPHABET_SIZE] = {{{0}}};
double Belief::charPosDensity[CHARS_PER_STRING][ALPHABET_SIZE][2] = {{{0}}};
double Belief::wordFreqs[N_WORDS] = {0};


double Belief::getPositionBelief(int pos){
    return trialBelief[pos+1]; // because trialBelief[0] is the trial-level post
}

double Belief::getMaxPositionBelief(int pos){
    double maxWordBelief = *std::max_element(wordBelief[pos], wordBelief[pos]+N_WORDS);
    double maxNonwordBelief = *std::max_element(nonwordBelief[pos], nonwordBelief[pos]+N_NONWORDS);
    // std::cout << maxWordBelief << " " << maxNonwordBelief << " " << 1-trialBelief[pos+1] << " "<< trialBelief[pos+1] << std::endl;
    double maxPosBelief = std::max(maxWordBelief*(1-trialBelief[pos+1]),maxNonwordBelief*trialBelief[pos+1]);
    // std::cout <<  maxPosBelief << std::endl; 
    return maxPosBelief;
}

double Belief::getTrialBelief(){
    return trialBelief[0]; 
}

Belief::~Belief(){
  if (_ownsRng){
    RNG::freeRng();
  }
}

double Belief::getConditioningBelief(int focus){
    if (conditioningPost == 0){
        return trialBelief[0];
    }
    else if(conditioningPost == 1){
        if(focus ==0){
            return 1;  // log(1)==0 so H has no effect
        }
        return trialBelief[focus];// because trialBelief[0] is the trial-level post, so this is prev string
    }
    else{
        #ifndef IGNORE_RCPP
            forward_exception_to_r(std::runtime_error("Invalid Conditioning Post!"));
        #else
            throw std::runtime_error("Invalid Conditioning Post!");
        #endif
    } 
    return 1; // should never get here, but passes -Wall -Werror :)
}

long double Belief::kahan_sum(double *vec, int size){
    // http://www.drdobbs.com/floating-point-summation/184403224?pgno=8
    long double sum, correction, corrected_next_term, new_sum;
    sum = vec[0];
    correction = 0.0L;
    for (int i = 1; i < size; i++)
    {
        corrected_next_term = vec[i] - correction;
        new_sum = sum + corrected_next_term;
        correction = (new_sum - sum) - corrected_next_term;
        sum = new_sum;
    }
    return sum;
}

void Belief::normalize(double *belief, int size){
    double sum = 0;
    sum = kahan_sum(belief, size); 
    for (int i = 0; i < size; i++){
        belief[i] = belief[i] / sum; 
    }
}

void Belief::updateCharPosDens(double samp[CHARS_PER_STRING][ALPHABET_SIZE], double noise){
    for (int i = 0; i < CHARS_PER_STRING; i++){
        for(int j = 0; j < ALPHABET_SIZE; j++){
            // pdf with mean zero
            charPosDensity[i][j][0] = gsl_ran_gaussian_pdf(samp[i][j], sdOfGaussSum(noise, _updateNoise)); 
            // pdf with mean 1 (z=mu+x, so with a different mu we do z-x, I think)
            charPosDensity[i][j][1] = gsl_ran_gaussian_pdf(samp[i][j]-1, sdOfGaussSum(noise, _updateNoise)); 
        }
    }
}



void Belief::populateStringCharDens(){
    for (int i = 0; i < N_WORDS; i++){
        for (int j = 0; j < CHARS_PER_STRING; j++){
            for (int k = 0; k < ALPHABET_SIZE; k++){
                wordCharDensity[i][j][k] = charPosDensity[j][k][wordCodes[i][j][k]]; 
            }
        }
    }
  // Populate nonword Char densities
    for (int i = 0; i < N_NONWORDS; i++){
        for (int j = 0; j < CHARS_PER_STRING; j++){
            for (int k = 0; k < ALPHABET_SIZE; k++){
                nonwordCharDensity[i][j][k] = charPosDensity[j][k][nonwordCodes[i][j][k]]; 
            }
        }
    }
}

void Belief::calcStringPosteriors(int pos){
    double prod; 
  // Calc word posterior
    for (int i = 0; i < N_WORDS; i++){
        prod = 1;
        for (int j = 0; j < CHARS_PER_STRING; j++){
            for (int k = 0; k < ALPHABET_SIZE; k++){
                prod = prod * wordCharDensity[i][j][k]; 
            }
        }
        wordBelief[pos][i] = wordBelief[pos][i] * prod;
    }
  // Calc nonword posterior
    for (int i = 0; i < N_NONWORDS; i++){
        prod = 1;
        for (int j = 0; j < CHARS_PER_STRING; j++){
            for (int k = 0; k < ALPHABET_SIZE; k++){
                prod = prod * nonwordCharDensity[i][j][k]; 
            }
        }
        nonwordBelief[pos][i] = nonwordBelief[pos][i] * prod;
    }
}

void Belief::calcTrialPosterior(int pos){
    double totalWordLik=0, totalNonwordLik = 0; 
    //wPostSum = 0; 
    for (int i=0; i<N_WORDS; i++){
        totalWordLik += wordBelief[pos][i];
    }
    for (int i=0; i<N_NONWORDS; i++){
        totalNonwordLik += nonwordBelief[pos][i]; 
    }
    for (int i=0; i<=STRINGS_PER_TRIAL; i++){
        if(i!=(pos+1)){ // again, trialBelief[0] is trial-level, whereas position is zero-index
            trialBelief[i] = trialBelief[i] * totalWordLik; 
        }
        else {
            trialBelief[i] = trialBelief[i] * totalNonwordLik; 
        }
    }
}

void Belief::noisifySample(double samp[CHARS_PER_STRING][ALPHABET_SIZE]){
    for (int i=0; i < CHARS_PER_STRING; i++) {
        for (int j = 0; j < ALPHABET_SIZE; j++){
            samp[i][j] = samp[i][j] + RNG::rnorm(0, _updateNoise);
        }
    } 
}

void Belief::bayesUpdate(double samp[CHARS_PER_STRING][ALPHABET_SIZE], int pos, double likelihoodNoise){
  noisifySample(samp); 
  updateCharPosDens(samp, likelihoodNoise);
  populateStringCharDens();
  calcStringPosteriors(pos);
  calcTrialPosterior(pos);
  normalize(wordBelief[pos], N_WORDS);
  normalize(nonwordBelief[pos], N_NONWORDS);
  normalize(trialBelief, STRINGS_PER_TRIAL+1); 
}

Belief::Belief(std::string *words, std::string *nonwords, double *wf, double trialWordPrior, int conditioningPost, double updateNoise): trialWordPrior(trialWordPrior), conditioningPost(conditioningPost), _updateNoise(updateNoise){
    _ownsRng = false;
    if(!RNG::rngIsActive()){
        RNG::setupRng();
        _ownsRng = true; 
    }
    // Generate all the string codes
    for (int i = 0; i < N_WORDS; i++){
        for (int j = 0; j < CHARS_PER_STRING; j++){
            for (int k = 0; k < ALPHABET_SIZE; k++){
                wordCodes[i][j][k] = CHAR_TO_BITVECTOR[words[i][j]-'a'][k];
            }
        }
    }
    for (int i = 0; i < N_NONWORDS; i++){
        for (int j = 0; j < CHARS_PER_STRING; j++){
            for (int k = 0; k < ALPHABET_SIZE; k++){
                nonwordCodes[i][j][k] = CHAR_TO_BITVECTOR[nonwords[i][j]-'a'][k];
            }
        }
    }    
    // reset nonword prior
    for (int i = 0; i<STRINGS_PER_TRIAL; i++){
        for(int j = 0; j < N_NONWORDS; j++){
            nonwordBelief[i][j] = 1/N_NONWORDS; 
        }
    }
    // Copy word frequencies in and hang on to them (for resetting priors)
    for (int i=0; i<N_WORDS; i++){
        wordFreqs[i] = wf[i];
    }
    // reset word prior:
    // first, normalize freqs -- this is our prior
    nonwordStringProb = (double)1/N_NONWORDS; // cast once, reuse.
    normalize(wordFreqs, N_WORDS); 
    reset(); // this populates wordBelief and trialBelief from the freq priors
}

void Belief::reset(){
    // this happens on new trials! not every word (because we're now tracking word belief over all the strings in the word). 

    for (int i = 0; i<STRINGS_PER_TRIAL; i++){
        for (int j = 0; j <N_WORDS ; j++){
            wordBelief[i][j] = wordFreqs[j]; 
        }
    }
    for (int i = 0; i<STRINGS_PER_TRIAL; i++){
        for (int j = 0; j <N_NONWORDS ; j++){
            nonwordBelief[i][j] = nonwordStringProb;
        }
    }
    trialBelief[0] = trialWordPrior; 
    for (int i = 1; i <= STRINGS_PER_TRIAL; i++){ 
        trialBelief[i] = trialWordPrior/STRINGS_PER_TRIAL;
    }
}

void Belief::setUpdateNoise(double updateNoise){
    _updateNoise = updateNoise; 
}


#ifndef IGNORE_RCPP
using namespace Rcpp; 

void Belief::updateCharPosDensForR(Rcpp::NumericMatrix samp, double noise){
    double s[CHARS_PER_STRING][ALPHABET_SIZE]; 
    for (int i = 0; i<CHARS_PER_STRING; i++){
        for (int j = 0; j<ALPHABET_SIZE; j++){
            s[i][j] = samp(i,j); 
        }
    }
    updateCharPosDens(s, noise);
}

Belief::Belief(SEXP words, SEXP nonwords, SEXP freqs, SEXP params){
    _ownsRng = false;
    if(!RNG::rngIsActive()){
        RNG::setupRng();
        _ownsRng = true; 
    }
    List par(params); 
    List wds(words);
    List nwds(nonwords);
    NumericVector wfreqs(freqs);
    trialWordPrior = as<double>(par["trialWordPrior"]);
    conditioningPost = as<double>(par["conditioningPost"]);    
    _updateNoise = as<double>(par["updateNoise"]); 
    std::string charRepr;
    for (int i = 0; i < N_WORDS; i++){
        charRepr = as<std::string>(wds(i)); 
        for (int j =0; j < CHARS_PER_STRING; j++){
            for(int k = 0; k<ALPHABET_SIZE; k++){
                wordCodes[i][j][k] = CHAR_TO_BITVECTOR[charRepr[j]-'a'][k];
            }
        }
    }
    for (int i = 0; i < N_NONWORDS; i++){
        charRepr = as<std::string>(nwds(i)); 
        for (int j =0; j < CHARS_PER_STRING; j++){
            for(int k = 0; k<ALPHABET_SIZE; k++){
                nonwordCodes[i][j][k] = CHAR_TO_BITVECTOR[charRepr[j]-'a'][k];
            }
        }
    }
    for (int i = 0; i<N_WORDS; i++){
        wordFreqs[i] = wfreqs[i];
    }
    normalize(wordFreqs, N_WORDS);
    nonwordStringProb = (double)1/N_NONWORDS; // cast once, reuse.
    reset();
}

void Belief::bayesUpdateForR(NumericMatrix samp, int pos, double likelihoodNoise){
    // just a way for passing in the sample as an R object
    double s[CHARS_PER_STRING][ALPHABET_SIZE]; 
    for (int i = 0; i<CHARS_PER_STRING; i++){
        for (int j = 0; j<ALPHABET_SIZE; j++){
            s[i][j] = samp(i,j); 
        }
    }
    bayesUpdate(s, pos, likelihoodNoise); 
}


NumericVector Belief::normalizeR(NumericVector v){
    int s = v.size();
    double *o = new double[s];
    NumericVector z(s); 
    for (int i=0; i<s; i++){
        o[i] = v(i);
    }
    normalize(o, s);
    for (int i=0; i<s; i++){
        z(i) = o[i];
    }
    return z; 
}

NumericMatrix Belief::wordBeliefGetterForR(){
    NumericMatrix o(STRINGS_PER_TRIAL, N_WORDS);
    for (int i=0; i<STRINGS_PER_TRIAL; i++){
        for (int j=0; j<N_WORDS; j++){
            o(i,j) = wordBelief[i][j];
        }
    }
    return o;
}

NumericMatrix Belief::nonwordBeliefGetterForR(){
    NumericMatrix o(STRINGS_PER_TRIAL, N_NONWORDS);
    for (int i=0; i<STRINGS_PER_TRIAL; i++){
        for (int j=0; j<N_NONWORDS; j++){
            o(i,j) = nonwordBelief[i][j];
        }
    }
    return o;
}

List Belief::wcdGetterForR(){
    List o(N_WORDS);
    for (int i = 0; i<N_WORDS; i++){
        NumericMatrix s(CHARS_PER_STRING, ALPHABET_SIZE); 
        for (int j = 0; j<CHARS_PER_STRING; j++){
            for (int k =0; k<ALPHABET_SIZE; k++){
                s(j,k) = wordCharDensity[i][j][k];
            }  
        }
        o[i] = s; 
    }
    return o;
}

List Belief::ncdGetterForR(){
    List o(N_NONWORDS);
    for (int i = 0; i<N_NONWORDS; i++){
        NumericMatrix s(CHARS_PER_STRING, ALPHABET_SIZE); 
        for (int j = 0; j<CHARS_PER_STRING; j++){
            for (int k =0; k<ALPHABET_SIZE; k++){
                s(j,k) = nonwordCharDensity[i][j][k];
            }  
        }
        o[i] = s; 
    }
    return o;
}

List Belief::cpdGetterForR(){
    List o(2);
    for (int i = 0; i<2; i++){
        NumericMatrix s(CHARS_PER_STRING, ALPHABET_SIZE); 
        for (int j = 0; j<CHARS_PER_STRING; j++){
            for (int k =0; k<ALPHABET_SIZE; k++){
                s(j,k) = charPosDensity[j][k][i];
            }  
        }
        o[i] = s; 
    }
    return o;
}

XPtr<Belief> Belief::returnPointerForR(){
  return XPtr<Belief>(this, false); 
}

RCPP_MODULE(belief_mod){
// eventually might want to expose everything for testing.  For now, expose just public stuff

  class_<Belief>("Belief")

  .constructor<SEXP,SEXP,SEXP,SEXP>("Maintains and updates beliefs")

  .property("wordBelief", &Belief::wordBeliefGetterForR)
  .property("nonwordBelief", &Belief::nonwordBeliefGetterForR)  
  .property("wcd", &Belief::wcdGetterForR)
  .property("ncd", &Belief::ncdGetterForR)
  .property("cpd", &Belief::cpdGetterForR)

  .method("normalizeTest", &Belief::normalizeR)
  .method("bayesUpdate", &Belief::bayesUpdateForR)
  .method("updateCPD", &Belief::updateCharPosDensForR)
  .method("populateSCD", &Belief::populateStringCharDens)
  .method("calcStringPost", &Belief::calcStringPosteriors)
  .method("calcTrialPost", &Belief::calcTrialPosterior)
  .method("getPositionBelief", &Belief::getPositionBelief)
  .method("getTrialBelief", &Belief::getTrialBelief)
  .method("reset", &Belief::reset)
  .method("getPtr", &Belief::returnPointerForR)
  ;
}
#endif
