// include guard
#ifndef RNG_H
#define RNG_H

#include <gsl/gsl_randist.h>
#include <stdexcept>
#include <unistd.h>  // for getpid() 
#include <time.h> // for time(NULL) on windows

#ifndef IGNORE_RCPP
	#include <Rcpp.h>
#endif

class RNG{
	private: 
		static gsl_rng * r;  /* global generator */     
		static bool rngCreated;
		
	public: 
		static bool rngIsActive(); 
		static void setupRng();				
		static void setupSeededRng(long seed); 
		static void freeRng(); // 		
		static gsl_rng * getRng(); 
		static double rGamma(double m, double s); 
		static double rnorm(double m, double s); 
		static int runif(int n); 
		static double rbernoulli(double p); 

};

#endif
