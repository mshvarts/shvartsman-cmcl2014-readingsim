#include "sampler.h"

double Sampler::samp[CHARS_PER_STRING][ALPHABET_SIZE] = {{0}};
double Sampler::trueString[CHARS_PER_STRING][ALPHABET_SIZE] = {{0}};
double Sampler::mem[MEMORY_SIZE][CHARS_PER_STRING][ALPHABET_SIZE] = {{{0}}};

void Sampler::drawPerceptSample(){
    for (int i=0; i < CHARS_PER_STRING; i++) {
        for (int j = 0; j < ALPHABET_SIZE; j++){
            samp[i][j] = trueString[i][j] + RNG::rnorm(0, perceptNoise);
            mem[memIndex][i][j] = samp[i][j];
        }
    }
    memIndex++;
//    return *samp;
}

void Sampler::setPerceptNoise(double pn){
  perceptNoise = pn;
}
void Sampler::setMemNoise(double mn){
  memNoise = mn;
}

void Sampler::drawMemSample(){
    int memSampSelected = RNG::runif(memIndex);
    for (int i=0; i < CHARS_PER_STRING; i++) {
        for (int j=0; j < ALPHABET_SIZE; j++){
            samp[i][j] = mem[memSampSelected][i][j] + RNG::rnorm(0, memNoise);
        }
    }
//    return *samp;
}

double Sampler::getPerceptNoise(){
    return perceptNoise;
}
 double Sampler::getMemNoise(){
    return memNoise;
}


Sampler::Sampler(double perceptNoise, double memNoise): perceptNoise(perceptNoise), memNoise(memNoise), memIndex(0){
    ownsRng = false;
    if(!RNG::rngIsActive()){
        RNG::setupRng();
        ownsRng = true;
  }
}


Sampler::~Sampler(){
  if (ownsRng){
    RNG::freeRng();
  }
}

void Sampler::reset(std::string newString){
    // Set the new string we're sampling from
    for (int i = 0; i < CHARS_PER_STRING; i++){
        stringRepr[i] = newString[i];
    }
    // Convert it to bitvector. Taking efficiency hit here a little bit by doing a conversion on each new string, but gaining a lot in clarity (since we're not needing to keep track of the full set of bitvector'd strings anywhere excpet in the Belief class). Can do testing to see if this matters a lot.
    stringToBitvector();
    // Reset memory index (no need to zero out memory -- just overwrite it as needed)
    memIndex = 0;
}


void Sampler::stringToBitvector(){
    for (int i = 0; i < CHARS_PER_STRING; i++){
        for(int j = 0; j<ALPHABET_SIZE; j++){
            trueString[i][j] = CHAR_TO_BITVECTOR[stringRepr[i]-'a'][j];
        }
    }
}

#ifndef IGNORE_RCPP
using namespace Rcpp;
Sampler::Sampler(SEXP params){
  List par(params);
  perceptNoise = as<double>(par["perceptNoise"]);
  memNoise = as<double>(par["memNoise"]);
  memIndex = 0;
  ownsRng = false;
  if(!RNG::rngIsActive()){
        RNG::setupRng();
        ownsRng = true;
  }
}

NumericMatrix Sampler::sampGetterForR(){
    NumericMatrix s(CHARS_PER_STRING, ALPHABET_SIZE);
    for (int i = 0; i<CHARS_PER_STRING; i++){
        for (int j = 0; j<ALPHABET_SIZE; j++){
            s(i,j) = samp[i][j];
        }
    }
    return s;
}

NumericMatrix Sampler::trueStringGetterForR(){
     NumericMatrix s(CHARS_PER_STRING, ALPHABET_SIZE);
    for (int i = 0; i<CHARS_PER_STRING; i++){
        for (int j = 0; j<ALPHABET_SIZE; j++){
            s(i,j) = trueString[i][j];
        }
    }
    return s;
}

List Sampler::memoryGetterForR(){
    List m(memIndex);
    for (int i = 0; i<memIndex; i++){
        NumericMatrix s(CHARS_PER_STRING, ALPHABET_SIZE);
        for (int j = 0; j<CHARS_PER_STRING; j++){
            for (int k =0; k<ALPHABET_SIZE; k++){
                s(j,k) = mem[i][j][k];
            }
        }
        m[i] = s;
    }
    return m;
}

XPtr<Sampler> Sampler::returnPointerForR(){
  return XPtr<Sampler>(this, false);
}

double sampPtrTest(SEXP ptr){
  XPtr< Sampler > sampPtr(ptr);
  return sampPtr->getPerceptNoise();
}

RCPP_MODULE(sampler_mod){
// eventually might want to expose everything for testing.  For now, expose just public stuff

  class_<Sampler>("Sampler")

  .constructor<SEXP>("Sets sampler noise params")

.property("samp", &Sampler::sampGetterForR)
.property("truestring", &Sampler::trueStringGetterForR)
.property("memory", &Sampler::memoryGetterForR)
.method("drawPerceptSample", &Sampler::drawPerceptSample)
.method("drawMemSample", &Sampler::drawMemSample)
.method("getPerceptNoise", &Sampler::getPerceptNoise)
.method("getMemNoise", &Sampler::getMemNoise)
.method("reset", &Sampler::reset)
.method("getPtr", &Sampler::returnPointerForR)
  ;
function("sampPtrTest", &sampPtrTest);
}
 #endif
