#include "rng.h"

bool RNG::rngCreated = false; 
gsl_rng * RNG::r; 

void RNG::setupRng(){
	if(rngCreated==true){
		#ifndef IGNORE_RCPP
			forward_exception_to_r(std::logic_error("Attempting to allocate RNG when it's already allocated (unseeded)!"));
		#else
			throw std::logic_error("Attempting to allocate RNG when it's already allocated!");
		#endif
	}
	const gsl_rng_type * T;
	long seed;
	T = gsl_rng_default;
	r = gsl_rng_alloc(T);
	seed = time(NULL) * getpid();     // insurance against correlated parallel runs
	gsl_rng_set(r, seed);
	rngCreated = true; 
}

void RNG::setupSeededRng(long seed){
	if(rngCreated==true){
		#ifndef IGNORE_RCPP
			forward_exception_to_r(std::logic_error("Attempting to allocate RNG when it's already allocated (seeded)!"));
		#else
			throw std::logic_error("Attempting to allocate RNG when it's already allocated!");
		#endif
	}
	const gsl_rng_type * T;
	T = gsl_rng_default;
	r = gsl_rng_alloc(T);
	if(r==NULL){
		#ifndef IGNORE_RCPP
			forward_exception_to_r(std::runtime_error("Failed to create RNG!"));
		#else
			throw std::runtime_error("Failed to create RNG!");
		#endif
	}
	gsl_rng_set(r, seed);
	rngCreated = true; 
}

void RNG::freeRng(){
	if(rngCreated==false){
		#ifndef IGNORE_RCPP
			forward_exception_to_r(std::logic_error("Attempting to free RNG when it's not created!"));
		#else
			throw std::logic_error("Attempting to free RNG when it's not created!");
		#endif
	}
	if(r==NULL){
		#ifndef IGNORE_RCPP
			forward_exception_to_r(std::runtime_error("rngCreated is true but RNG pointer is NULL! Did R garbage-collect it?"));
		#else
			throw std::runtime_error("rngCreated is true but RNG pointer is NULL! Did R garbage-collect it?");
		#endif
	}
	gsl_rng_free(r); 
	rngCreated = false; 
}

double RNG::rGamma(double m, double s)
{
	if(rngCreated==false){
		#ifndef IGNORE_RCPP
			forward_exception_to_r(std::logic_error("Attempting to generate a random number with no RNG allocated!"));
		#else
			throw std::logic_error("Attempting to generate a random number with no RNG allocated!"); 
		#endif
	}
  if (m==0) {
    return(0);
  }
  double k = (m*m) / (s*s);      // shape k = m^2/s^2
  double theta = (s*s) / m;     // scale theta = s^2 / m
  return gsl_ran_gamma(r,k, theta); 
}

bool RNG::rngIsActive(){
	return rngCreated; 
}

double RNG::rnorm(double m, double s){
	if(rngCreated==false){
		#ifndef IGNORE_RCPP
			forward_exception_to_r(std::logic_error("Attempting to generate a random number with no RNG allocated!"));
		#else
			throw std::logic_error("Attempting to generate a random number with no RNG allocated!"); 
		#endif
	}
	return m + gsl_ran_gaussian_ziggurat(r, s); 
}

int RNG::runif(int max){
	if(rngCreated==false){
		#ifndef IGNORE_RCPP
			forward_exception_to_r(std::logic_error("Attempting to generate a random number with no RNG allocated!"));
		#else
			throw std::logic_error("Attempting to generate a random number with no RNG allocated!");
		#endif
	}
	return gsl_rng_uniform_int(r, max); 
}

double RNG::rbernoulli(double p){
	if(rngCreated==false){
		#ifndef IGNORE_RCPP
			forward_exception_to_r(std::logic_error("Attempting to generate a random number with no RNG allocated!"));
		#else
			throw std::logic_error("Attempting to generate a random number with no RNG allocated!");
		#endif
	}
	return gsl_ran_bernoulli(r, p); 
}

#ifndef IGNORE_RCPP
using namespace Rcpp; 
RCPP_MODULE(rng_mod){
	function("setupRng", &RNG::setupRng);
	function("setupSeededRng", &RNG::setupSeededRng);
	function("rbernoulli", &RNG::rbernoulli, List::create(_["p"]=0.5));
	function("rnorm", &RNG::rnorm, List::create(_["m"] = 0.0, _["s"] = 1.0), "Generates random deviate from gaussian distribution");
	function("runif", &RNG::runif, List::create(_["max"] = 1));
	function("rGamma", &RNG::rGamma, List::create(_["m"] = 0.0, _["s"] = 1.0));
	function("rngIsActive", &RNG::rngIsActive);
	function("freeRng", &RNG::freeRng);
}
#endif
