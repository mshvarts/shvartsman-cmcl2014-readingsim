// include guard
#ifndef TRIAL_H
#define TRIAL_H



// includes we need
#include "constants.h"
#include <string>
#include <vector>
#include <iostream> // for dumpState()
#include "architecture.h"
#include "belief.h"
#include "sampler.h"
#include "policy.h"
#include "recorder.h"
#include "helpers.h" // currently just for sd of a sum of gaussians
#include <math.h> // what is this for?
#include <algorithm> // for std::fill

#ifndef IGNORE_RCPP
    #include <Rcpp.h>
#endif


class Trial{
    public:
        Trial(Architecture *arch, Policy *pol, Sampler *sampler, Belief *belief, Recorder *recorder, bool storeDroppedSamples=true);
        void run(std::string *ts, int trialtype, bool withTrace=false);
        void runWithTrace(std::string *ts, int trialType);
        void reset();
        #ifndef IGNORE_RCPP            // Trial(SEXP archParams, SEXP polParams, SEXP samplerParams, SEXP beliefParams, SEXP words, SEXP nonwords, SEXP freqs, Rcpp::List trialParams);
            Trial(SEXP Rin);
            void runForR(SEXP ts, int trialType);
            Rcpp::List getTrace();
        #endif

    private:
        Architecture * _arch;
        Policy * _pol;
        Sampler * _sampler;
        Belief * _belief;
        Recorder * _recorder;
        bool _storeDroppedSamples;
        int _sampleToThreshold(int wIndex, SamplingTypes sType, int maxSamps=MAX_SAMPS);
        void _bufferSamples(int n);
        std::vector<double> _eblInStart;
        std::vector<double> _sampDropStart;
        std::vector<double> _volitionalPerSampStart;
        std::vector<double> _freePerSampStart;
        std::vector<double> _memSampStart;
        std::vector<double> _sptStart;
        std::vector<double> _setStart;
        std::vector<double> _eblOutStart;
        std::vector<double> _eblInEnd;
        std::vector<double> _sampDropEnd;
        std::vector<double> _volitionalPerSampEnd;
        std::vector<double> _freePerSampEnd;
        std::vector<double> _memSampEnd;
        std::vector<double> _sptEnd;
        std::vector<double> _setEnd;
        std::vector<double> _eblOutEnd;
        std::vector<int> _perSampsToThreshold;
        std::vector<int> _memSampsToThreshold;
        std::vector < double > _beliefTrace;
        double _motorStart;
        double _motorEnd;
        int _response;
        bool _responseInitiated;
        double _pNoise, _pmNoise;
        bool _withTrace;
        // double maxTrialDuration;
        // TODO: bring timePerStep back into the interface so that R can pass it in
        // double timePerStep;
        std::string * _trialStrings;
        // double trialTime;
        // double sfd;
        // int _nFixations;
        // int perSampToDecision, perSampCount, memSampCount, droppedSampCount;
        // std::vector < std::vector<double > > stateTrace;
        // std::vector < std::vector<double > > actionTrace;
        // int traceIndex;
        // int focus; // where are the eyes?
        // int samplingFrom; // where are we eligible to sample from?
        // bool eblInF, eblOutF, sptF, setF, perSampF, memSampF;
        // int motorStage;
        // double motorTime;
        // int eyeAction;
        // int motorAction;
        // double eblInTimer, eblOutTimer, sptTimer, setTimer, eblOutNext;
        // int sampType;
        // int response;
};

#endif
