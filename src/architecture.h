// include guard
#ifndef ARCH_H
#define ARCH_H

// no forward declarations needed

// includes we need
#ifndef IGNORE_RCPP
    #include <Rcpp.h>
#endif
#include "rng.h" // this is wrapping the RNG
#include "constants.h" // for TIMEPERSTEP for rounding

class Architecture {

 public:
    double drawEBL();
    double drawSET();
    double drawSPT();
    double drawMotorTime();
    bool withPerceptualPrimacy(); 
    void setSPT(double spt);
    void setPerPrim(bool perPrim); 
    double getTimePerStep(); 
    Architecture(double eyeBrainLagMean, double saccadeExecTimeMean, double saccadeProgTimeMean, double motorTimeMean, double coefVariation, bool PerceptualPrimacy, double timePerStep);
    ~Architecture();
    #ifndef IGNORE_RCPP
    Rcpp::XPtr<Architecture> returnPointerForR();
    Architecture(SEXP params);
    #endif

 private:
    double eyeBrainLagMean;
    double saccadeExecTimeMean;
    double saccadeProgTimeMean;
    double motorTimeMean;
    double coefVariation;    
    bool perceptualPrimacy; 
    double timePerStep; 
    bool ownsRng; 
};


#endif
