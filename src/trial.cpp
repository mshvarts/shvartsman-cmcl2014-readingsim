#include "trial.h"


Trial::Trial(Architecture *arch, Policy *pol, Sampler *sampler, Belief *belief, Recorder *recorder, bool storeDroppedSamples): _arch(arch), _pol(pol), _sampler(sampler), _belief(belief), _recorder(recorder), _storeDroppedSamples(storeDroppedSamples), _eblInStart(STRINGS_PER_TRIAL, 10000), _sampDropStart(STRINGS_PER_TRIAL, 10000), _volitionalPerSampStart(STRINGS_PER_TRIAL, 10000), _freePerSampStart(STRINGS_PER_TRIAL, 10000), _memSampStart(STRINGS_PER_TRIAL, 10000), _sptStart(STRINGS_PER_TRIAL, 10000), _setStart(STRINGS_PER_TRIAL, 10000), _eblOutStart(STRINGS_PER_TRIAL, 10000), _eblInEnd(STRINGS_PER_TRIAL, 10000), _sampDropEnd(STRINGS_PER_TRIAL, 10000), _volitionalPerSampEnd(STRINGS_PER_TRIAL, 10000), _freePerSampEnd(STRINGS_PER_TRIAL, 10000), _memSampEnd(STRINGS_PER_TRIAL, 10000), _sptEnd(STRINGS_PER_TRIAL, 10000), _setEnd(STRINGS_PER_TRIAL, 10000), _eblOutEnd(STRINGS_PER_TRIAL, 10000), _perSampsToThreshold(STRINGS_PER_TRIAL, 10000), _memSampsToThreshold(STRINGS_PER_TRIAL, 10000) {
    _pNoise = sampler->getPerceptNoise();
    _pmNoise = sdOfGaussSum(sampler->getPerceptNoise(), sampler->getMemNoise());
}

void Trial::reset(){
    for (int i=0; i<STRINGS_PER_TRIAL; i++){
        _eblInStart[i] = -10000;
        _sampDropStart[i] = -10000;
        _volitionalPerSampStart[i] = -10000;
        _freePerSampStart[i] = -10000;
        _memSampStart[i] = -10000;
        _sptStart[i] = -10000;
        _setStart[i] = -10000;
        _eblOutStart[i] = -10000;
        _eblInEnd[i] = -10000;
        _sampDropEnd[i] = -10000;
        _volitionalPerSampEnd[i] = -10000;
        _freePerSampEnd[i] = -10000;
        _memSampEnd[i] = -10000;
        _sptEnd[i] = -10000;
        _setEnd[i] = -10000;
        _eblOutEnd[i] = -10000;
        _perSampsToThreshold[i] = -10000;
        _memSampsToThreshold[i] = -10000;
    }
    _motorStart = -10000;
    _motorEnd = -10000;
    _response = -10000;
    _responseInitiated = false;
    _belief->reset();
    if (_withTrace) _beliefTrace.clear(); 
}

void Trial::_bufferSamples(int n){
    for (int i=0; i<n; i++){
        _sampler->drawPerceptSample(); // sampler stores each sample automatically
    }
}

int Trial::_sampleToThreshold(int wIndex, SamplingTypes sType, int maxSamps){
    // int nSamps = 0;
    // while(nSamps < maxSamps && (!_pol->moveEyes(_belief->getMaxPositionBelief(wIndex), sType))){
    int nSamps;
    for (nSamps = 0; nSamps < maxSamps; nSamps++){
        if (!_responseInitiated && _pol->respond(_belief->getTrialBelief())){
            _responseInitiated = true;
            // TODO: consider passing in samp process start and getting rid of all this logic
            switch (sType){
                case Perception:
                    _motorStart = _volitionalPerSampStart[wIndex] + (nSamps * _arch->getTimePerStep());
                    break;
                case FreePerception:
                    _motorStart = _freePerSampStart[wIndex]+ (nSamps * _arch->getTimePerStep());
                    break;
                case Memory:
                    _motorStart = _memSampStart[wIndex] + (nSamps * _arch->getTimePerStep());
                    break;
            }
            _motorEnd = _motorStart + _arch->drawMotorTime();
            _response = _belief->getTrialBelief()>0.5 ? 1 : 0;
        }
        if (_pol->moveEyes(_belief->getMaxPositionBelief(wIndex), sType, nSamps)) break;
        switch (sType){
            case Perception:
                _sampler->drawPerceptSample();
                _belief->bayesUpdate(_sampler->samp, wIndex, _pNoise);
                break;
            case FreePerception:
                _sampler->drawPerceptSample();
                _belief->bayesUpdate(_sampler->samp, wIndex, _pNoise);
                break;
            case Memory:
                _sampler->drawMemSample();
                _belief->bayesUpdate(_sampler->samp, wIndex, _pmNoise);
                break;
        }
        if(_withTrace){
            // beliefTrace is a single vector but we know its ncol (it's strings per trial +1) so we
            // convert it back to matrix only when we need to. It makes filling it easier here, and makes
            // sending it to R easier in getTrace()
            _beliefTrace.push_back(_belief->getTrialBelief());
            for (int i=0; i < STRINGS_PER_TRIAL; i++){
                _beliefTrace.push_back(_belief->getMaxPositionBelief(i));
            }
        }
    }
    if (nSamps == MAX_SAMPS) {
        throw std::runtime_error("Sampled to MAX_SAMPS! If you meant to go this long, up MAX_SAMPS!");
    }
    return nSamps;
}

void Trial::run(std::string *ts, int trialType, bool withTrace){
    _withTrace = withTrace;
    _trialStrings = ts;
    reset();
    // seed EBLIn
    _eblInStart[0] = 0;
    _eblInEnd[0] = _eblInStart[0] + _arch->drawEBL();
    int wIndex=0;
    for (wIndex=0; wIndex < STRINGS_PER_TRIAL; wIndex++){
        _sampler->reset(_trialStrings[wIndex]);
    // sampDrop --
    //     Start: when eblIn is over
    //     End: if primacy, end = start. otherwise, when max(eblIn, prevMemSamp) is over, except on word 1 where it's eblIn
    // if end < start, end=start
        _sampDropStart[wIndex] = _eblInEnd[wIndex];
        if (_arch->withPerceptualPrimacy()){
            _sampDropEnd[wIndex] = _sampDropStart[wIndex];
        } else {
            _sampDropEnd[wIndex] = wIndex == 0 ? _eblInEnd[wIndex] : fmax(_eblInEnd[wIndex], _memSampEnd[wIndex-1]);
        }
        _sampDropEnd[wIndex] = _sampDropEnd[wIndex] < _sampDropStart[wIndex] ? _sampDropStart[wIndex] : _sampDropEnd[wIndex];
        int sampDropCount = static_cast<int>((_sampDropEnd[wIndex]-_sampDropStart[wIndex]) / TIMEPERSTEP);
        // TODO: consider swapping _withTrace for #ifndef IGNORE_RCPP or somesuch
        if (!_withTrace) _recorder->saveDroppedSamps(sampDropCount);
        if(_storeDroppedSamples){
            _bufferSamples(sampDropCount);
        }
    // volitional perSamp
    //     Start: when sampDrop is over
    //     End: when threshold is reached
        _volitionalPerSampStart[wIndex] = _sampDropEnd[wIndex];
        _perSampsToThreshold[wIndex] = _sampleToThreshold(wIndex, Perception);
        _volitionalPerSampEnd[wIndex] = _volitionalPerSampStart[wIndex] + _perSampsToThreshold[wIndex] * TIMEPERSTEP;
        if (!_withTrace)  _recorder->saveSampsToDecision(_perSampsToThreshold[wIndex], wIndex);

    // SPT
    //     Start: when threshIsReached
    //     End: when sptOver
        _sptStart[wIndex] = _volitionalPerSampEnd[wIndex];
        _sptEnd[wIndex] = _sptStart[wIndex] + _arch->drawSPT();
    // SET
    //     Start: when sptOver
    //     End: when setOver
        _setStart[wIndex] = _sptEnd[wIndex];
        _setEnd[wIndex] = _setStart[wIndex] + _arch->drawSET();
        if (!_withTrace) _recorder->recordFixationEnd(_setStart[wIndex] - _eblInStart[wIndex], wIndex);
        _freePerSampStart[wIndex] = _volitionalPerSampEnd[wIndex];
    // we need to know eblInNext for some cutoff stuff
        if (wIndex== (STRINGS_PER_TRIAL-1)){// if we're on the last word, don't index into the future
        // eblOut
        //     Start: when setOver
        //     End: on the last word, primacy from next word can't come into play
        //     if End > start, end = start
            _eblOutStart[wIndex] = _sptEnd[wIndex];
            double eblInDur = _eblInEnd[wIndex] - _eblInStart[wIndex];
            _eblOutEnd[wIndex] = _eblOutStart[wIndex] + eblInDur;
            // TODO: consider making _motorStart here equal to sampling end
            _motorStart = _sptStart[wIndex];
            _motorEnd = _motorStart + _arch->drawMotorTime();
            _response = _belief->getTrialBelief()>0.5 ? 1 : 0;
            _responseInitiated = true;
            _freePerSampEnd[wIndex] = _eblOutEnd[wIndex];
        } else {
        // eblIn (next)
        // Start: when last SET is over (or 0 if we're on the first word)
        // End: when eblIn is over
            _eblInStart[wIndex+1] = _setEnd[wIndex];
            _eblInEnd[wIndex+1] = _eblInStart[wIndex+1] + _arch->drawEBL();
        // eblOut
        //     Start: when setOver
        //     End: if primacy, when max(eblOut, eblInNext) is over. Otherwise when eblOut is over
        //     if End > start, end = start
            _eblOutStart[wIndex] = _sptEnd[wIndex];
            double eblInDur = _eblInEnd[wIndex] - _eblInStart[wIndex];
            if(_arch->withPerceptualPrimacy()){
                _eblOutEnd[wIndex] = fmin(_eblInEnd[wIndex+1], _eblOutStart[wIndex]+eblInDur);
            } else {
                _eblOutEnd[wIndex] = _eblOutStart[wIndex] + eblInDur;
            }
            // free perSamp
            //     Start: when volitional perSamp is over
            //     End: if primacy, when max(eblOut, eblInNext) is over. Otherwise, when eblOut is over
            //     if End > start, end = start
            // But: we already set eblOut to max(eblOut, eblInNext) so don't need to do the check here again
            // if (_arch->withPerceptualPrimacy()){
                // _freePerSampEnd[wIndex] = fmin(_eblOutEnd[wIndex], _eblInStart[wIndex+1]);
            // } else {
                // _freePerSampEnd[wIndex] = _eblOutEnd[wIndex];
            // }
        }
        _freePerSampEnd[wIndex] = _eblOutEnd[wIndex];
        int nFreeSamples = static_cast<int>( (_freePerSampEnd[wIndex]-_freePerSampStart[wIndex]) / TIMEPERSTEP );
        _sampleToThreshold(wIndex, FreePerception, nFreeSamples);
        if (!_withTrace) _recorder->savePerSamps(nFreeSamples+_perSampsToThreshold[wIndex]);

    // memSamp
    //     Start: when free perSamp is over
    //     End: if primacy, when max(threshReachTime, eblInNext) is over
    //     if End > start, end = start
        _memSampStart[wIndex] = _freePerSampEnd[wIndex];
        if (_arch->withPerceptualPrimacy()){
            double maxMemSamps = static_cast<int>((_eblInEnd[wIndex+1] - _memSampStart[wIndex])/ TIMEPERSTEP); 
            _memSampsToThreshold[wIndex] = _sampleToThreshold(wIndex, Memory, maxMemSamps);
        } else {
            _memSampsToThreshold[wIndex] = _sampleToThreshold(wIndex, Memory);
        }
        _memSampEnd[wIndex] = _memSampStart[wIndex] + _memSampsToThreshold[wIndex] * TIMEPERSTEP;

        if (!_withTrace) _recorder->saveMemSamps(_memSampsToThreshold[wIndex]);
        if (_responseInitiated) break; // if we responded during this word
    }
    if (!_withTrace) _recorder->recordTrialEnd(_motorEnd, _response, wIndex, wIndex);
}

#ifndef IGNORE_RCPP
using namespace Rcpp;


void Trial::runForR(SEXP ts, int trialType){
    List stringVec(ts);
    std::string tsLocal[STRINGS_PER_TRIAL];
    for (int i =0; i < STRINGS_PER_TRIAL; i++){
        tsLocal[i] = as<std::string>(stringVec[i]);
    }
    run(tsLocal, trialType, true); // just run with trace by default
}

List Trial::getTrace(){
    // for some reason, DataFrame::create() fails with a linking error. so we do this the hard way:
    List wordLevelTrace(18);
    wordLevelTrace(0) = _eblInStart;
    wordLevelTrace(1) = _sampDropStart;
    wordLevelTrace(2) = _volitionalPerSampStart;
    wordLevelTrace(3) = _freePerSampStart;
    wordLevelTrace(4) = _memSampStart;
    wordLevelTrace(5) = _sptStart;
    wordLevelTrace(6) = _setStart;
    wordLevelTrace(7) = _eblOutStart;
    wordLevelTrace(8) = _eblInEnd;
    wordLevelTrace(9) = _sampDropEnd;
    wordLevelTrace(10) = _volitionalPerSampEnd;
    wordLevelTrace(11) = _freePerSampEnd;
    wordLevelTrace(12) = _memSampEnd;
    wordLevelTrace(13) = _sptEnd;
    wordLevelTrace(14) = _setEnd;
    wordLevelTrace(15) = _eblOutEnd;
    wordLevelTrace(16) = _perSampsToThreshold;
    wordLevelTrace(17) = _memSampsToThreshold;
    wordLevelTrace.attr("class") = "data.frame";
    // via http://gallery.rcpp.org/articles/faster-data-frame-creation/
    GenericVector sample_row = wordLevelTrace(0);
    StringVector row_names(sample_row.length());
    for (int i = 0; i < sample_row.length(); ++i) {
        char name[5];
        sprintf(&(name[0]), "%d", i);
        row_names(i) = name;
    }
    wordLevelTrace.attr("row.names") = row_names;

    StringVector col_names(wordLevelTrace.length());
    col_names(0) = "eblInStart";
    col_names(1) = "sampDropStart";
    col_names(2) = "volitionalPerSampStart";
    col_names(3) = "freePerSampStart";
    col_names(4) = "memSampStart";
    col_names(5) = "sptStart";
    col_names(6) = "setStart";
    col_names(7) = "eblOutStart";
    col_names(8) = "eblInEnd";
    col_names(9) = "sampDropEnd";
    col_names(10) = "volitionalPerSampEnd";
    col_names(11) = "freePerSampEnd";
    col_names(12) = "memSampEnd";
    col_names(13) = "sptEnd";
    col_names(14) = "setEnd";
    col_names(15) = "eblOutEnd";
    col_names(16) = "perSampsToThreshold";
    col_names(17) = "memSampsToThreshold";

    wordLevelTrace.attr("names") = col_names;

    NumericMatrix bTrace(STRINGS_PER_TRIAL+1, _beliefTrace.size()/(STRINGS_PER_TRIAL+1), _beliefTrace.begin());

    // NumericMatrix bTrace(_beliefTrace.size()/(STRINGS_PER_TRIAL+1), STRINGS_PER_TRIAL+1);

    // NumericVector bTrace

    // int i=0;
    // for(int r=0; r < bTrace.nrow(); r++){
    //     for (int c=0; c < bTrace.ncol(); c++){
    //         bTrace(r,c) = _beliefTrace[i++];
    //     }
    // }

    return List::create(_("wordLevelTrace")=wordLevelTrace, _("beliefTrace")=bTrace, _("motorStart")=_motorStart, _("motorEnd")=_motorEnd, _("response")=_response);
    // return List::create(_("wordLevelTrace")=wordLevelTrace,  _("motorStart")=_motorStart, _("motorEnd")=_motorEnd, _("response")=_response);
}

Trial::Trial(SEXP RIn): _eblInStart(STRINGS_PER_TRIAL, 10000), _sampDropStart(STRINGS_PER_TRIAL, 10000), _volitionalPerSampStart(STRINGS_PER_TRIAL, 10000), _freePerSampStart(STRINGS_PER_TRIAL, 10000), _memSampStart(STRINGS_PER_TRIAL, 10000), _sptStart(STRINGS_PER_TRIAL, 10000), _setStart(STRINGS_PER_TRIAL, 10000), _eblOutStart(STRINGS_PER_TRIAL, 10000), _eblInEnd(STRINGS_PER_TRIAL, 10000), _sampDropEnd(STRINGS_PER_TRIAL, 10000), _volitionalPerSampEnd(STRINGS_PER_TRIAL, 10000), _freePerSampEnd(STRINGS_PER_TRIAL, 10000), _memSampEnd(STRINGS_PER_TRIAL, 10000), _sptEnd(STRINGS_PER_TRIAL, 10000), _setEnd(STRINGS_PER_TRIAL, 10000), _eblOutEnd(STRINGS_PER_TRIAL, 10000), _perSampsToThreshold(STRINGS_PER_TRIAL, 10000), _memSampsToThreshold(STRINGS_PER_TRIAL, 10000) {
    List in(RIn);
    // TODO: make this access by name, not by position
    SEXP archParams = in[0];
    SEXP polParams = in[1];
    SEXP samplerParams = in[2];
    SEXP beliefParams = in[3];
    SEXP words = in[4];
    SEXP nonwords = in[5];
    SEXP freqs  = in[6];
    List trialParams = as<List>(in[7]);
    if (trialParams.containsElementNamed("storeDroppedSamples")){
        _storeDroppedSamples = as<bool>(trialParams["storeDroppedSamples"]);
    } else {
        _storeDroppedSamples = false;
    }
    _arch = new Architecture(archParams);
    _pol = new Policy(polParams);
    _sampler = new Sampler(samplerParams);
    // belief and architecture is a bit tricky
    _belief = new Belief(words, nonwords, freqs, beliefParams);
    _recorder = new Recorder(freqs, words, nonwords);
    _pNoise = _sampler->getPerceptNoise();
    _pmNoise = sdOfGaussSum(_sampler->getPerceptNoise(), _sampler->getMemNoise());
    // std::cout << _pNoise << " "<< _pmNoise << std::endl;
}


RCPP_MODULE(trial_mod){
// eventually might want to expose everything for testing.  For now, expose just public stuff
  class_<Trial>("Trial")
  .constructor<SEXP>("Sets up trial")
  .method("run", &Trial::runForR)
  .method("getTrace", &Trial::getTrace)
  ;
}

#endif
