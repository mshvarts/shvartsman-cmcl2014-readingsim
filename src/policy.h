// include guard
#ifndef POLICY_H
#define POLICY_H

// no forward declarations needed

// includes we need
#include <cmath> // for fmax()
#include "constants.h" // to grab MAX_THRESH
#include <gsl/gsl_sf_log.h> // for H(post)
#ifndef IGNORE_RCPP
    #include <Rcpp.h>
#endif

class Policy {

 public:

    bool isMemoryModel();
    Policy(double saccadeA, double saccadeB, double memoryA, double memoryB, double taskYes, double taskNo, int sampDeadline);
    bool moveEyes(double wordBelief, SamplingTypes sType, int nSamps);
    bool respond(double trialBelief);
    // double getSaccadeThresh(Belief* b);
    // double getMemoryThresh(Belief* b);
    // double getTaskThresh(Belief* b);

    #ifndef IGNORE_RCPP
        // Rcpp::List getActionsForR(double stringBelief, double trialBelief, double conditioningBelief, int sampType);
        Policy(SEXP params);
        Rcpp::XPtr<Policy> returnPointerForR();
    #endif

 private:
    // these should be const... but then we can't initialize from our R interface
    double maxThresh;
    double saccadeA;
    double saccadeB;
    double memoryA;
    double memoryB;
    double taskYes;
    double taskNo;
    //int conditioningPost;
    double _H(double posterior);
    int _sampDeadline; 
};

#endif
