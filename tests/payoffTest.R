require(readingSim)
require(testthat)

#install.packages('~/Dropbox/Submissions/prelim_sub/modelv2/cCode/readingSim_1.0.tar.gz', repos=NULL, INSTALL_opts = '--no-multiarch', type='source')
context('Tests that the payoff class is working properly')

calc.payoffs <- function(rt, acc, ps, pa){
	p <- ps * (5000-rt)/1000 - pa * (1-acc)
	return(p)
}

payoffMod <- Module('payoff_mod', 'readingSim')

expect_equal(payoffMod$getPayoff(3284, 1,2), calc.payoffs(3284, 1, 8, 150))

expect_equal(payoffMod$getPayoff(3284, 0,2), calc.payoffs(3284, 0, 8, 150))

