require(readingSim)
require(testthat)

#install.packages('~/Dropbox/Submissions/prelim_sub/modelv2/cCode/readingSim_1.0.tar.gz', repos=NULL, INSTALL_opts = '--no-multiarch', type='source')
context('Tests that the RNG is working properly')

rngMod = Module('rng_mod', 'readingSim')

test_that('RNG can be created and destroyed', {
	expect_that(rngMod$rngIsActive(), is_false())
	rngMod$setupRng()
	expect_that(rngMod$rngIsActive(), is_true())
	rngMod$freeRng() 
	expect_that(rngMod$rngIsActive(), is_false())
	})


test_that('RNGs return values close to benchmark R rng values', {
	rngMod$setupRng()
	cpp <- mean(replicate(10000, rngMod$rnorm()))
	r <- mean(rnorm(10000))
	expect_equal(cpp, r, scale=NULL, tolerance=0.05)

	cpp <- sd(replicate(10000, rngMod$rnorm()))
	r <- sd(rnorm(10000))
	expect_equal(cpp, r, scale=NULL, tolerance=0.05)

	cpp <- mean(replicate(10000, rngMod$runif(100)))
	r <- mean(runif(10000, max=100))
	expect_equal(cpp, r, scale=NULL, tolerance=0.05)

	cpp <- mean(replicate(10000, rngMod$rGamma(10, 5)))
	r <- mean(rgamma(10000, shape=10^2/5^2, scale=5^2/10))
	expect_equal(cpp, r, scale=NULL, tolerance=0.05)
	
	cpp <- sd(replicate(10000, rngMod$rGamma(10, 5)))
	r <- sd(rgamma(10000, shape=10^2/5^2, scale=5^2/10))
	expect_equal(cpp, r, scale=NULL, tolerance=0.05)
	
	rngMod$freeRng()
})

test_that('Seeded RNG returns identical results each time',{
	rngMod$setupSeededRng(10)
	stream1 <- replicate(10000, rngMod$rnorm())
	rngMod$freeRng()
	rngMod$setupSeededRng(10)
	stream2 <- replicate(10000, rngMod$rnorm())
	rngMod$freeRng()
	expect_equal(stream1, stream2)
})
	
test_that('Unseeded RNG returns different results each time',{
	rngMod$setupRng()
	stream1 <- replicate(10000, rngMod$rnorm())
	rngMod$freeRng()
	Sys.sleep(1) # since we seed off system time 
	rngMod$setupRng()
	stream2 <- replicate(10000, rngMod$rnorm())
	rngMod$freeRng()
	expect_false(all(stream1==stream2))
})

test_that('Errors are raised when we try to generate numbers without RNG active, allocate RNG when one is allocated, or free an RNG when one is not allocated', {
	expect_error(rngMod$freeRng())
	expect_error(rngMod$rnorm())
	expect_error(rngMod$runif())
	expect_error(rngMod$rbernoulli())
	expect_error(rngMod$rGamma())
	rngMod$setupRng()
	expect_error(rngMod$setupRng())
	rngMod$freeRng()
})