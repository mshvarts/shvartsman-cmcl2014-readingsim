library(Rcpp)
library(testthat)

sourceCpp('../static/lexicon.cpp')


test_that("Word and nonword lists don't overlap", {
all.kf <- read.table('../data/kf-words.txt')
all.arc <- read.table('../data/arc-nonwords.txt')
expect_false(any(all.kf$V1 %in% all.arc$V1))
})

list1 <- loadWordsR('../data/kf-words.txt',1500)
Sys.sleep(1)
list2 <- loadWordsR('../data/kf-words.txt',1500)

expect_false(all(list1$words==list2$words))
expect_false(all(list1$words==list2$freqs))

list1 <- loadNonwordsR('../data/arc-nonwords.txt', 10366)
Sys.sleep(1)
list2 <- loadNonwordsR('../data/arc-nonwords.txt', 10366)
expect_false(all(list1==list2))

